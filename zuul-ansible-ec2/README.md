# Setting up Zuul with Ansible on Amazon EC2

## Overview

The current solution uses Ansible for instantiating the AWS instance and setting up Zuul and Nodepool on a single machine.  There are defaults for use with our private AWS account, which can be overridden.  In the future this may well be changed to a terraform mechanism in alignment with other celduin infrastructure.

The output is a Zuul configuration with a set of parameters to point at a known instance of Gerrit, with an SSH key to add to the known Gerrit manually.

## Prerequisites

Firstly you need to install the following python packages:

```bash
pip3 install boto
pip3 install boto3
```

Secondly, you need to have the appropriate credentials file for your AWS account in `~/.ssh/`.  The default is `CelduinInfra.pem`, this can be obtained from several members from the Celduin team.  *Note* that this can be overridden in the command to execute the playbook, covered later in this guide.

You may get issues running against the credential file if it has too many access rights, to fix this run the following command:

```bash
chmod 600 ~/.ssh/<filename>
```

You may need to run this under sudo, depending on permissions for the .ssh folder.

If needed make a new project in your Gerrit instance (and add it to the task `Make main.conf` in `roles/zuul/tasks/main.yml`).

## Instantiating Zuul-CI

Before the run, you will need to edit `group_vars/all/zuul.yml` to change the zuul_gerrit_host` and `zuul_gerrit_port` to point to your Gerrit server, and the `gerrit_password` to the one obtained from the _GENERATE NEW PASSWORD_ button in the _HTTP Credentials_ section on the Gerrit config page for the zuul user. (Note that generating a new password will invalidate previous passwords.)

The defaults for these are described later in this section, as is the command to override at playbook execution time.

From the zuul-ansible-ec2 directory, run the following command for the default setup:

```bash
ansible-playbook --ask-vault-pass startup.yml
```

The vault password (used for EC2 credentials) is known by following people in the project:

  - Paul Adshead (@paul.adshead)
  - Chris Phang (@christopherphang)
  - Phil Dawson (@phildawson)

Should you wish to override some of the default parameters, the command becomes the following:

```bash
ansible-playbook --ask-vault-pass startup.yml -e "<var1=value varN=value>"
```

Where each variable you are overriding is separated by a space, the parameters of most interest are detailed in the table below:

| Variable | Description | Default Value |
|:---------|:------------|:--------------|
|gerrit_password|The password for accessing the Gerrit instance|Hidden value, see the information on the vault below|
|ec2_instance_type|The EC2 machine type that Zuul & Nodepool services will be running on|t2.micro|
|ec2_nodepool_instance_type|The EC2 machine type that Nodepool will spin up when required|t2.micro|
|ec2_ssh_key_name|The name of the credentials in .ssh/ for accessing the AWS account|CelduinInfra|
|ansible_ssh_private_key_file|Full path to the SSH key for accessing created AWS instances|~/.ssh/CelduinInfra.pem|
|preferred_ami|Preferred AWS Machine Image, in our case Debian 10|ami-08fe9ea08db6f1258|
|admin_user|AWS login, if not root, for Ubuntu images this would be 'ubuntu'|admin|
|ec2_region|AWS Server Region|eu-west-2|
|ec2_vpc_id|Preferred Virtual Cloud to associate instance with|vpc-d8e18fb0|
|ec2_subnet_id|Preferred AWS subnet to associate the instance with|subnet-2259e558|
|zuul_gerrit_host|Gerrit instance address|ec2-35-177-249-221.eu-west-2.compute.amazonaws.com|
|zuul_gerrit_port|TCP/IP Port to access the Gerrit instance|8080|
|zuul_instance_name|The name given to the instance of Zuul running in AWS|ansible-zuul-main|

Configuration items are found in the files in `group_vars/all/*`

If the playbook hangs at `wait_for_connection`, check your `/etc/ansible/hosts` for stray entries.

"Roles" in `startup.yml` are executed in sequence, with each role file (in `roles/<name>/tasks/main.yml`) containing tasks which are executed in sequence.  Data associated with the roles are in `roles/<name>/files/` and `roles/<name>/templates/`.  Variable substitution happens using _jinja2_ templating.

At the end of the run, Ansible will spit out an ssh public key block, which you should paste into to the _ssh keys_ section of the Gerrit config for the zuul user.

eg.

```bash
TASK [zuul : SSH public key that needs to be registered on Gerrit] *************
ok: [18.132.35.206] => {
    "gerrit_key.public_key": "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHkwmNUZ9RTqcHmZtqwv3j/uKV6a9MTMpdOmVTO2T2er"
}
```

## End Result

Following a successful execution of the playbook, the following will exist in AWS:

```mermaid
graph TD
  subgraph Gerrit Instance*
    GD[(Storage)]
    GS[Gerrit Server]
    GS--Configured Access---GD
  end
  subgraph Zuul Instance
    ZS[Scheduler]
    ZZ[Zookeeper]
    NP[Nodepool]
    ZW[Web]
    ZG[Gearman]
    ZM[Merger]
    ZE[Executor]
    ZF[Finger]
    ZD[(SQL<br>Database)]
    ZZ---NP
    ZS---ZZ
    ZW---ZZ
    ZG---ZS
    ZE---ZG
    ZF---ZE
    ZF---ZG
    ZM---ZG
    ZW---ZE
    ZW---ZG
    ZS---ZD
    ZW---ZD
  end
  ZS--Configured Repository-->GS
```

*The Gerrit Instance is not spun up by the playbooks currently, but does have an impact on configuration.

The Zuul dashboard can be accessed on port 9000 of the EC2 instance created by
the play. At the moment, the easiest way to access it is by looking up the
public DNS of the instance from the AWS dashboard.

## Debugging tools

Log into your Zuul server using

```
ssh -i ~/.ssh/CelduinInfra.pem admin@<IP.ADD.RE.SS>>
```

The Zuul logs are in `/var/log/zuul/`.  Most useful are
`scheduler.log` and `executor.log`.  Nodepool logs are in
`/var/log/nodepool/nodepool.log`.

If you wish to debug a job:

```
sudo -u zuul zuul-executor verbose
sudo -u zuul zuul-executor keep
```

and allow the job to run.  The results will be inside
`/var/lib/zuul/builds/`.

To turn this off:

```
sudo -u zuul zuul-executor unverbose
sudo -u zuul zuul-executor nokeep
```

If you wish a worker instance to linger after the job has completed.

```
sudo -u zuul \
 zuul autohold --tenant CelduinAnsible --project <project name> \
 --job <build name> --reason "<something>"
```

When the job has completed, run:

```
sudo -u zuul nodepool list --detail
```

to find out the IP address of the instance, and log in using

```
ssh -i ~zuul/.ssh/nodepool_rsa admin@<instance_ip>
```

To remove the instance, execute

```
sudo -u zuul nodepool delete <ID>
```

The autohold will only apply once.  You will need to delete it before
re-enabling it:

```
sudo -u zuul zuul autohold-list
sudo -u zuul zuul autohold-delete <ID>
```

## Terminating the Zuul instance

In order to tear down the Zuul instance set up using the default configuration, run

```
absible-playbook --ask-vault-pass teardown.yml
```

This will remove all the AWS resources used to set up the Zuul instance.

If the Zuul instance you're taring down was set up using none-default values for
any of the AWS related configuration, you will have to pass those values to the
teardown playbook in the same way.
