# Identity Access Management

This document will describe the Identity Access Management (referred as [IAM]) configuration that will be used across different Cloud Providers.  

The term [IAM] covers many elements related to identity and access management.  
The first element coming to mind is usually user account and access. But this also encompass ideas such as Groups, Service Accounts, Roles, etc.

It exist many different set of name/term/acronyms to reference those elements, making it hard to have discussions spanning across different providers/platforms/tools. As the goal of this project is to covers different Cloud Providers (see the [Cloud Architecture Design](./#cloud-architecture-design.md) document for more details about the list) compromises will have to be done regarding the terminology employed.  
However clarity and the ease of access to the knowledge remains our primary goal therefore we will do our best to use a terminology which is not technology dependant and in places where specific terms will be used explanations/links/clarifications will be provided.

## Index

Even if the configurations will be consistent, and therefore are expected to be fairly similar, it is not excluded that adjustments will have to be done in places to reflect the particularities of a given Cloud Provider; this is why each Cloud Provider will have its dedicated section.

  + [General Structure](#general-structure)
  + [AWS](#aws)

## General Structure

The [principle of least privilege] will be followed throughout the configuration. We will be using four elements to define the project hierarchy:  

  + Users  
    Each user represent an physical person working on this project.  
  + [Groups](#groups)  
    Each group is a set of users with similar responsibilities and access.  
  + [Roles](#roles)  
    Roles are the representation of a very narrow power, certain groups will have access to different roles. Having access to a role does not imply that you can perform this power continuously you need to aquire the role (usually for a given period of time) in order to peform the action the role grants you.  
  + [Service accounts](#service-accounts)  
    A service account is similar to a user for the only exception that it is not associated to a physical person but a service or an application.  


### Groups

In light of the previous introduction 5 groups have been defined:  

```mermaid
graph TD;
  Root-->Admin;
  Admin-->Dev;
  Dev-->Guest;
  Billing;
```

The following sections will describe each groups in more details. Please note that for most of the groups users from a group will have access to a superset of the rights from the group bellow them, to represent this the sections bellow are presenting those groups from the bottom-up.


#### Guest

This is the least privileged group. Members of this group can only view a given sub-set of resources.  
The main purpose for this group is to permit users to visualise current resources without risking of security breaches (access to secrets) or accidental damage done to the infrastructure.  

As very little understanding of the project is required for this group this will be the entry point for any new user. It can be also used on a regular basis by other users when access to elevated right aren't required.

This is the list of allowed actions for this group:  

  + Read only to compute resources (clusters, VM)  
  + Read only to storage resources  


#### Dev

The members of the Dev group should be able to perform simple day to day actions which carries minimal risks to the infrastructure and requires little to no understanding of the whole project.  
They have access to the following actions:  

  + Manipulate existing resources  
    - Modify resource configuration (cluster, VM)  
    - Modify storage (e.g: add/remove object to/from bucket stores)
  + [Guest](#guest)  


#### Admin

Users in this group should have the following rights:  

  + [IAM] management  
    - Create/Delete rules/roles/groups/users  
    - Add/Remove users from groups/roles  
  + [Dev](#dev)  
  + [Guest](#guest)  


#### Root

This is actually not a group but a single account. This account has admin access to all the elements of the structure, can't be deleted and can override anything.  
The sole purpose of this account is for disaster recovery and therefore should never be use on a regular basis or extended period of time.  

Considering the weight such responsibilities represent it should be shared among a small number of persons with the connections details recorded in a safe location (with preferably backup versions stored in multiple other locations).


#### Billing

This group is different from the others as it exists for administrative usage. It has visibility on budgeting and cost monitoring side.

Access to this group will be given on an add-hoc basis depending the users needs, with once again a focus on the [principle of least privilege].


### Roles

Currently we have only one Role defined: Resource Manager.

This Role gives access to Create/Delete Cloud resources. It should be mainly used by Service accounts through automated processes; however the Admin group will have access to this role in order to be able to perform maintenance when some unexpected behavior occurs.  
The use of this Role should be kept to a minimum and only for brief periods of time as this grants user a big set of power over the Cloud resources and could lead to accidental damages or cost increase.


### Service Accounts

We will have a single service account which will be used by the CI pipeline to provision/update/destroy cloud resources. In order to perform those action it will be grant the Resource Manager role.


## AWS

This section describes the AWS mapping of the [general structure](#general-structure) defined above.


### Policies

This section will list the different Policies and list the different API calls each has access to.
The list of each Policies definitions can be found in the file named after the said Policy:  

  + [Billing-Reader](#aws-iam/Billing-Reader.md)  
  + [Bucket-Reader](#aws-iam/Bucket-Reader.md)  
  + [Bucket-Viewer](#aws-iam/Bucket-Viewer.md)  
  + [Bucket-Object-Editor](#aws-iam/Bucket-Object-Editor.md)  
  + [S3-Bucket-Creator](#aws-iam/S3-Bucket-Creator.md)  
  + [EC2-Resource-viewer](#aws-iam/EC2-Resource-viewer.md)  
  + [EC2-Resource-editor](#aws-iam/EC2-Resource-editor.md)  
  + [EC2-Resource-Creator](#aws-iam/EC2-Resource-Creator.md)  


### Groups

As described above there are currently four Groups defined:

  + [Billing](#billing)  
    - [Billing-Reader](#aws-iam/Billing-Reader.md)  
  + [Guest](#guest)  
    - [Bucket-Reader](#aws-iam/Bucket-Reader.md)  
    - [EC2-Resource-viewer](#aws-iam/EC2-Resource-viewer.md)  
  + [Developers](#dev)  
    - [Bucket-Reader](#aws-iam/Bucket-Reader.md)  
    - [Bucket-Object-Editor](#aws-iam/Bucket-Object-Editor)  
    - [EC2-Resource-viewer](#aws-iam/EC2-Resource-viewer.md)  
    - [EC2-Resource-editor](#aws-iam/EC2-Resource-editor.md)  
  + [Administrator](#admin)  
    - ToDo


### Roles

We currently have one Role:

  + [Resource-Creator](#roles)  
    - [Bucket-Reader](#aws-iam/Bucket-Reader.md)  
    - [Bucket-Object-Editor](#aws-iam/Bucket-Object-Editor.md)  
    - [S3-Bucket-Creator](#aws-iam/S3-Bucket-Creator.md)  
    - [EC2-Resource-viewer](#aws-iam/EC2-Resource-viewer.md)  
    - [EC2-Resource-editor](#aws-iam/EC2-Resource-editor.md)  
    - [EC2-Resource-Creator](#aws-iam/EC2-Resource-Creator.md)  






[//]: # (EXTERNAL LINKS)

[IAM]: https://en.wikipedia.org/wiki/Identity_management
[principle of least privilege]: https://en.wikipedia.org/wiki/Principle_of_least_privilege
