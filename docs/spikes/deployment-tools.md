# Deployment Tools Spike

This objective from this spike is to figure out which tool would be the best suited to help us deploy to Kubernetes.

Issue: #67

# Context

The use case for the deployment tool is described in the Issue as follow:


We want to be able to deploy our Kubernetes definitions in a reliable / reproducible / open manner.
The Process of deploying should be independent from how the definitions are created.
Some of the key points are:

+ Must be able to handle several environments (currently we have 4)
+ Handle redeploying Pods when a new version of their Docker image is created.
+ Integrate with a CI Pipeline
+ Should be able to deploy locally or in the Cloud
+ Shold have or integrate with some sort of access management
+ Should be FOSS


---

The tools considered where:

+ [Tilt]  
  This tool claims to be easy to use and promise to help with local development
+ [Argo-CD]  
+ [Rules-k8s]  


## Tilt

Tilt seems fairly simple to use and have a clear interface to help navigate.  
It relies on a server deployed in the cluster which monitors files to deploy, it also supports Helm or Kustomize).


When it notices a change Tilt will try to apply them immediately, if it fails to apply changes Tilt will simply not change a thing and won't retry to deploy it unless it is instructed otherwise.  
While testing this functionality I noticed that it was struggling to pick up a change of `namespace` leaving the application deployed in the previous `namespace`, this is an edge case and I did not investigate further than dummy proving the set-up.

It is configured via Starlark language and can integrate with Bazel and [Rules-k8s].  
Tilt will check a `Tiltfile` to know what files to track, it mostly work with file names but can also call scripts to generate Yaml files.

Removing elements from the `Tiltfile` `Tilt` doesn't remove the associated elements, I haven't found a solution to make it happen and suspect it is intended behaviour. While this is fine for short lived cluster I would worry about resources forgotten and left running in Live environments.

The main focus is set on deploying locally and I would require more testing before I could trust it to manage Live environments.

## Argo-CD


[Argo] is divided into several parts:
  + [Workflows]  
    This can be used to run containers in a given order (workflow). It would require us to rewrite our Kubernetes definitions to match. While this is not relevant to our current use case it is still an interesting tool.
  + [CD]  
    This tool helps keeping Kubernetes environments in sync with the expected difinition. It integrates with many tools such as HELM, Kustomize or Jsonnet
  + [Rollouts]  
    This toll adds two new strategies for deploying a changes (`Blue/Green`, `Canary`). This could be interesting when working at a bigger scale and performing system changes but it would not resolve our current usecase.
  + [Events]  
    Add support for events handling in Argo behaviour. Currently we have little use for this tool but could prove to be valulable when integrating with more complex systems


The main focus for us right now is on [CD] as it is the tool to deploy

As [Tilt] [Argo-CD] requires that some elements are deploy to the cluster.

While being very similar to [Tilt] it focuses more on deploying to remote cluster and has more options to increase resilience of the system compared to [Tilt].

Some of those elements are the possibility to define [Projects] to group resources, [Roles] with RBAC to limit actions preventing un-intended changes.

Another interesting point of [Argo-CD] is the separation between how to generate definitions and how to apply them (CI separated from CD).

Several options are available to define what [Argo-CD] should monitor (Commit SHA, Tag, Branch, Ref) it can be further reduced to a given folder.

While [Tilt] ignores resources that aren't defined [Argo-CD] considers those are orphaned. By default it won't act against those but it can be configured to remove them keeping only what is configured. This solve the issue from [Tilt] not being able to delete resources.

It claims to integrate with Nginx and Traefik ingress definitions (I haven't tested it as we currently don't use this kind of resources).

[Argo-CD] configuration is kept in Kubernetes as custom resource, making it possible to manage it the same way we manage our other Kubernetes resources, and ultimately let [Argo-CD] manage it.


## Rules k8s

It is part of Bazel. Some parts are still unstable (authentication, dependencies).

The rule itself it fairly simple to use as it focuses only on deploying Kubernetes configuration files.  
While it can be used to manage local development it requires a bit more of configuration to prevent using resources or deploying to the wrong environment.

It supports Jsonnet to generate definitions, and recommends separating the generation of the definition and when it is passed to the tool. Having separation between creating definitions and applying them helps improving traceability.

It also let us create `k8s-objects` to keep related elements groups and simplify managing large clusters.

This tool is quite powerful yet simple thanks to it's tight focus, however it also requires a good understanding of Bazel and some up-front configuration




[//]: # (EXTERNAL LINKS)

[Tilt]: https://tilt.dev/
[Tilt docs]: https://docs.tilt.dev/

[Argo]: https://argoproj.github.io/
[Workflows]: https://argoproj.github.io/projects/argo/
[CD]: https://argoproj.github.io/argo-cd/
[Argo-CD]: https://argoproj.github.io/argo-cd/
[Rollouts]: https://argoproj.github.io/argo-rollouts/
[Events]: https://argoproj.github.io/projects/argo-events
[Projects]: https://argoproj.github.io/argo-cd/user-guide/projects/

[Rules-k8s]: https://github.com/bazelbuild/rules_k8s
