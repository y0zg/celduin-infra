# Celduin Infrastructure Releases 

Currently we exist in a state of limbo, between fully automated and kind of
automated. This makes our release process somewhat complex. This document aims
to describe what to do.

## Criteria for release

End to end testing is currently done on our staging cluster. Staging must display the following characteristics.

- An end to end build of freedesktop-sdk and/or libreML must have occurred, with no failures in push and pull steps.
   - This should occur from a cold cache.
- An end to end build of abseil must have occurred, with no failures in push and pull steps.
   - This should occur from a cold cache.
- All web endpoints are accessible
- There are no observable errors on monitoring stacks. Places to look at include (but are not restricted to):
   - gRPC return codes
   - Any observable I/O, CPU, memory pressure
   - Suspicious log messages.

Unfortunately, much of this is manual right now. Issues such as #123 and #124 aim to make much of this process automatable.

Note that much of this could also be done in development environments. Please aim to have this as part of your checklist,
before merging to master.

## Process

## 1. Announce downtime
Announce the server downtime. This should be done in the `#celduin` channel on
freenode. Ideally announce the expected downtime if you can figure it out.

## 2. Run the terraform
Terraform currently needs to be manually run every time, as we haven't added CI
to run it for us yet. To run the terraform use:
```bash
./scripts/terraform-jsonnet -e prod -c apply
```
and a similar command for public.

Some caveats to be aware of:
* S3 buckets must be manually emptied (via the CLI or web UI) before deletion
* Terraform doesn't seem to be resizing instances in place, see #112

## 3. Tag the repository
Tag the repository with a new release tag. We use Semantic Versioning, so it's a
good idea to be familiar with the [semver specification]. What exactly is or is
not a breaking change is up for debate (#129), so use best judgement. As a rule
of thumb, if no artifacts are lost it's a bug fix, if artifacts are lost but no
API (that is, user-facing endpoints) changes then it's a minor release, if the
API changes then it's a major release.

Note however that we are currently in v0.x.y releases, so we use x as the major
point.

For tagging, you can either use the git cli or use the gitlab web UI. Of the
two, the web UI is probably simpler.

## 4. Restart any deployments that need restarting
Currently updating a configmap will not automatically redeploy a dependent
deployment. As a result, we need to restart any deployments that have had
configmaps change under them. The best way to do this to generate the json:
```
mkdir -p tmp
jsonnet -m tmp kubernetes/clusters/prod/deployment.jsonnet
```
and to then delete and create the offending deployment from the generated json.
```
kubectl delete -f tmp/foo-Deployment.json; kubectl apply -f tmp/foo-Deployment.json
```

This may cause Argo CD to consider these deployments out of sync. Ideally we
should fix #59.

### Artifact Storage
An additional concern is the artifact cache. Thanks to #37, it is a major issue
if the artifact cache points to blobs that do not exist in CAS. As a result, any
change to the storage backend which causes the loss of blobs will require a
clean out of the artifact cache.

Here kubernetes' StatefulSet abstraction becomes our enemy, as we can't simply
restart the deployment. Rather, we must enter the container and manually clear
out the artifacts. To do this use
```
kubectl exec -ti -n rex statefulset/storage-artifact -- bash
```
to shell into the artifact server, then
```
rm -rf /artifacts/cas/refs/heads/* /artifacts/artifacts/refs/*
```

The reason this has two locations is because BuildStream changed the server's
layout between 1.4 and master, `cas/refs/heads` is used for 1.4.x,
`artifacts/refs` is used for master.

## 5. Ensure that our test projects work with the newly deployed server

Follow the instructions in `test/buildstream` to make sure that we can push and
pull from both production servers.

## 6. Announce that the servers are back
Shout from the mountain tops that you have deployed to production, and it is (as
far as you know) working correctly. Again, #celduin on freenode is the go-to
mountain top for this.

## 7. Have a brew
Have a brew, you deserve it. In this time, you may also hope for there to be no
regressions or complications arising from the deployment.

[//]: # (EXTERNAL LINKS BLOCK)
[semver specification]: https://semver.org/
