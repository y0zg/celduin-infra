# Grafana

This document covers the basics of [Grafana] configuration and setup. It should help you in understanding the current configuration and help you get started using it in the Celduin infrastructure through some examples.  
If you are looking for information more centered to Grafana itself you can refer to [Grafana official documentation][grafana-documentation].

## Deploying Grafana

[Grafana] is composed of a single Kubernetes definition for the time being to add it to your cluster run:

```
kubectl apply -f kubernetes/monitoring/grafana/grafana.yaml
```

## Accessing Grafana Web UI

### Localy

Once [Grafana] is running in the cluster you can make it available at `localhost:3000` with the following command:

```
kubectl port-forward services/grafana 3000
```

### AWS

Our [grafana instance][prod-grafana] is available at https://stats.cache.aws.celduin.co.uk/grafana.
We also have grafana for [staging].


[//]: # (EXTERNAL LINKS)

[Grafana]: https://grafana.com/
[grafana-documentation]: https://grafana.com/docs/grafana/latest/
[prod-grafana]: https://stats.cache.aws.celduin.co.uk/grafana
[staging]: https://stats.staging.aws.celduin.co.uk/grafana
