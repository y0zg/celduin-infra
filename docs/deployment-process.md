# Automated Deployment Process

In this document you will find explanations on the process followed to deploy Kubernetes resources to the different environments.

# Context

This step is one of the last steps of the automated Infrastructure, at this step we have a set of Kubernetes resources defined that we want to deploy to a given cluster.  
At the current time we will make the following assumptions:

+ The Kubernetes definitions are valid.
+ The environment specified is the intended one (no check to prevent passing the wrong yaml files or pointing at the wrong environment).
+ The Cluster already exists and can support the definitions being passed.
+ Argo-CD is already running and configured properly.

# Secrets Exception

Currently we do not have a solution to let Argo-CD handle Kubernetes Secrets. Until a solution is implemented we will have to manually manage thoses resources.  
Only the certficates are Secrets and need to be handle manually.

# Generate Argo-CD Elements

You can find the different Jsonnet configuration in `/kubernetes/argocd`

## Application

To create a new Application you need to pass the following parameters:
+ name: str  
  This will be the name of the Application. Must be unique accross Argo-CD
+ project: str  
  Name of the Argo-CD Project to which the Application should belong
+ repo: str - defaut value: https://gitlab.com/celduin/infrastructure/infrastructure-declarations.git  
  Url of the Git repo where the configurations are held
+ targetRevision: str - defaut value: master  
  Target to monitor in the repository can be one of the following types: commit SHA, Tag, Branch
+ path: str - default value: ''  
  Path to the folder to monitor
+ server: str - default value: https://kubernetes.default.svc  
  Url of the server where to deploy the Application
+ namespace: str - default value: default  
  Namespace in which the Application should be deployed
+ finalizer: code - default value: true  
  When set to true Argo-CD will delete the Kubernetes elements when the Application is deleted


To create Grafana Application use the following command:
```
jsonnet --tla-str name="grafana" \
--tla-str project="default" \
--tla-code finalizer=true \
--tla-str path="kubernetes/monitoring/grafana" \
kubernetes/argo-cd/application.jsonnet > grafana.json
```

## Project

To create a new Project you need to provide the following parameters:
+ name: str  
  This will be the name of the Project. Must be unique accross Argo-CD
+ argoNamespace: str - default value: argocd  
  Namespace where Argo-CD is deployed
+ description: str  
  Description of the Project
+ sourceRepos: str - default value: https://gitlab.com/celduin/infrastructure/infrastructure-declarations.git
  Comma separated List of URL of repositories that can be monitored by this Project
+ destinations: str - default|https://kubernetes.default.svc
  Comma separated list of Argo CD Destinations, following this structure 'namespace|server' e.g: 'default|https://kubernetes.default.svc'


To create the Staging Project use the following command:
```
jsonnet --tla-str name="remote-cache" \
--tla-str project="default" \
--tla-code finalizer=true \
--tla-str path="kubernetes/rex" \
kubernetes/argo-cd/application.jsonnet > rex.json
```
