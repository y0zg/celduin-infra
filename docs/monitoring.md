# Monitoring
## Introduction
The purpose of this document is to explain:
  * The Monitoring Stack.
  * What each monitoring component covers.

## The Monitoring Stack Structure
The monitoring stack is composed by:
  *  [Grafana]
  *  [Jaeger]
  *  [Prometheus]

[Jaeger] and [Prometheus] will gather metrics from the Remote Execution
infrastructure and [Grafana] will present them in some fancy graphs in a
[dashboard].

## [Jaeger]
Jaeger is distributed tracing system, collects gRPC traces in the cluster.
gRPC traces allow us to gather insights in the network traffic between the
different application in the cluster. For more detail, read [grpc-tracing] document.

## [Prometheus]
Prometheus is an open-source systems monitoring and alerting toolkit. It works
by adding some labels to the Kubernetes objects you want to gather metrics from,
Prometheus will search the labelled objects in the cluster and scrape the metrics
once it finds them, storing them in a time series database.

To make this possible, the application running in those k8s objects must have
had implemented the Prometheus API on itself. Below there is a list of the
components we are currently monitoring and the information they can provide us:
  * BuildBarn:
    * grpc calls statistics.
    * blobstore statistics.
  * Nginx:
    * Connections statistics.
    * Http request.
    * If nginx is up.
  * Prometheus itself:
    * Http requests

For more detail read [prometheus-setup] document. Prometheus has a complementary
plugin called [Node Exporter].
### [Node Exporter]
Node exporter exports metrics from the host machine. Our intention is to use it
to obtain metrics from the nodes our infrastructure is running in.
[Node exporter] allows you to gather system levels metrics, such as:
  * CPU statistics.
  * Disk statistics.
  * Filesystem statistics.
  * Load average
  * IPVS status
  * Memory statistics.
  * Network statistics.
  * Scheduler statistics.

  [//]: # (EXTERNAL LINKS BLOCK)
  [Grafana]: https://grafana.com/
  [Dashboard]: https://stats.cache.aws.celduin.co.uk/grafana/?orgId=1
  [Jaeger]:  https://www.jaegertracing.io/
  [Grpc-tracing]: https://gitlab.com/celduin/infrastructure/celduin-infra/-/blob/master/docs/grpc-tracing.md
  [Prometheus]: https://prometheus.io/
  [Prometheus Expression Browser]: https://stats.cache.aws.celduin.co.uk/prometheus/graph?g0.range_input=1h&g0.expr=cpu&g0.tab=1
  [Node Exporter]: https://github.com/prometheus/node_exporter
  [Prometheus-setup]: https://gitlab.com/celduin/infrastructure/celduin-infra/-/blob/master/docs/prometheus-setup.md
