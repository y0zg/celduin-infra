# Remote Execution Deployment Design Document

This document aims to outline the design of a Remote Execution Server
deployment, which will be defined by the Kubernetes in this repository.

The intention of this document is to:
  * Help newcomers understand what is deployed and why
  * Track changes in the deployment and lessons learned

The intention of this document is NOT to:
  * Outline precisely the requirements for an enterprise deployment of a
    remote-execution service
  * Anticipate the implementation difficulties with any third party software
    requirements a deployment may have

## Index

  1. [Deployment Architecture](#deployment-architecture)
  2. [Components](#components)

## Deployment Architecture

We hope to deploy a [BuildBarn] instance that is robust, scalable and compatible
with [BuildStream], [Bazel] and [RECC]. This deployment will be based roughly
on those found at [bb-deployments]. Our application architecture will look
something like this, if all goes well:

```mermaid
graph LR;
  %% Nodes
  nginx[Nginx]:::loadBalancer;

  bb-forbidden[bb-storage Forbidden]:::service;
  bst-artifact-server[BuildStream Artifact Server]:::service;

  bb-storage-cas[bb-storage CAS and Action Cache]:::service;
  bb-storage-router[bb-storage gRPC demultiplexer]:::service;
  bb-scheduler[bb-scheduler]:::service;
  subgraph Worker
    bb-worker[bb-worker]:::service;
    bb-runner[bb-runner]:::service;
  end

  artifacts[(Artifact Store)]:::storage;
  objects[(On Disk Store)]:::storage;

  monitoring[Monitoring Stack]:::monitoring;

  %% Edges
  nginx -- build.bazel* --> bb-storage-router;
  nginx -- google.bytestream* --> bb-storage-router;
  nginx -- Auth Failed --> bb-forbidden
  nginx -- buildstream* --> bst-artifact-server --> artifacts;
  bb-storage-router --> bb-scheduler;
  bb-storage-router --> bb-storage-cas --> objects;
  bb-worker --> bb-runner;
  bb-worker --> bb-scheduler;
  bb-worker --> bb-storage-cas;
  bb-runner --> bb-worker;

  %% Styling
  classDef loadBalancer fill:#591,stroke:#333;
  classDef service fill:#f75,stroke:#333;
  classDef storage fill:#57f,stroke:#333;
  classDef monitoring fill:#ff0,stroke#333;
```

This should be the necessary architecture to achieve our goal for a robust
BuildBarn deployment.

For CRASH, we will also need an OSTree server, which should actually be a dumb
HTTP server. This is a detail we haven't decided on an implementation for yet.

We will use Kubernetes for the deployment, which gives us the advantage of
scalability, and meaning we don't have to be too fussy with cloud provider
configuration - Kubernetes just handles a lot of things. Ideally all of the
components defined in Kubernetes will be stateless and ephemeral. At present
we have some difficulties in making the bst-artifact-server stateless, which
is currently a single point of failure. We hope to improve this in future.


## Components

This section details the components, what they are for, and how they are
configured.

### Nginx

We use [Nginx] primarily to route the [gRPC] traffic for remote execution to the
relevant places. This is particularly useful for remote caching, where we need
to only allow certain clients to push to the cache. It should also serve as a
load balancer to the separate pods for the other services.

Nginx is the location of TLS termination, beyond here traffic is internal and
unencrypted. We don't decrypt on the infra load balancer level as we can't
guarantee that this will be available in all scenarios, and more importantly
doing so may mangle gRPC traffic.

Due to a restriction in how gRPC calls are made and how nginx gRPC routing
works, we can't prefix the call with an arbitrary string. This means that we
can't use `location` blocks as effectively, and must separate different
behaviours with different `server` blocks, and hence different DNS.

Nginx should also serve as the authentication and authorisation point, although
what form this will take is still TBD. Currently we have only a BuildStream
Artifact Server, which has push access restricted by TLS client authentication.

### bb-storage Forbidden

This is a bb-storage using the `error` blobstore backend to return a gRPC
`PERMISSION_DENIED` error. Calls that attempt to push to the pull cache are
routed here.

In an ideal world this component would be omitted, and we'd simply return the
error from nginx, but this is not possible at present.

### bst-artifact-server

This is a service used to store the artifact references for BuildStream
artifacts. Currently this is compatible with BuildStream 1.4 and the master
branch, but this may change soon. The artifact server is used to translate
cache keys into CAS digests (and some extra information for master).

Unfortunately this currently requires on disk storage, and is a single point
of failure for our deployment. We would like for this to be in an in-memory
store, like Redis, but this likely requires us writing our own service.

### bb-storage gRPC demultiplexer

This is essentially a router used to send information to either the scheduler
or the CAS.

### bb-storage CAS and ActionCache

The CAS is a Content Addressable Storage, which stores blobs named after a hash
of their content. This is used to cache the results of builds, whether they be
from Bazel, BuildStream or RECC. As the blobs are addressed by their content,
things are deduplicated to a file level, making this storage efficient. The
ActionCache is similar to the BuildStream Artifact Cache, but for Bazel
Actions. An action is a single step of the compilation process, e.g. a single
gcc invocation. The ActionCache maps actions to blob digests.

As bb-storage abstracts the blobstore used, the exact places we send blobs are
easily changed. Currently, we have a sharded on-disk solution to the storage,
where each pod is attached to an EBS volume used as a shard of storage. This
gives us a reasonably robust storage solution, in that we don't lose all data if
a shard is corrupted.

Our initial plan was to send large blobs to S3 and small blobs to Redis (on
Elasticache) for the first (AWS) phase. Unfortunately this proved fragile due to
Redis being temperamental (using it for medium-term data storage is far from
its intended use case) and S3 access times. The S3 access times were _such_ a
slow down that it was causing the nodes to OOM, as they couldn't push the blobs
fast enough. In the future, it may be worth evaluating Redis as a fast cache for
small blobs, and S3 as a long term data store.

### bb-scheduler

The bb-scheduler is the service that actually handles remote execution requests
and assigns jobs to workers.

### bb-worker

We intend to have a large number of bb-worker/bb-runner pairs, which actually
do the jobs. bb-workers communicate with the scheduler to get allocated jobs to
perform, and then tell the bb-runner what job to execute.

### bb-runner

The bb-runner is where compile actions are actually done. This is often used to
define the toolchain used for compilation, but we would prefer to do this using
bazel toolchains. BuildStream may also experience difficulties here as it uses
a sandbox to stage only the dependencies it has as a unique toolchain.
Hopefully [BuildBox] will solve this for us.

### Monitoring Stack

The monitoring stack will necessarily also be deployed by Kubernetes, but the
exact components and flow are not yet decided.

[//]: # (EXTERNAL LINKS BLOCK)


[//]: # (REMOTE EXECUTION)
[Buildstream]: https://www.buildstream.build/
[Buildbarn]: https://github.com/buildbarn
[Bazel]: https://bazel.build/
[RECC]: https://gitlab.com/bloomberg/recc
[bb-deployments]: https://github.com/buildbarn/bb-deployments
[BuildBox]: https://gitlab.com/buildgrid/buildbox/
[gRPC]: https://grpc.io

[//]: # (WEBSERVER)
[Nginx]: https://nginx.com
