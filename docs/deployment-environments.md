We have several different deployments of our infrastructure, the purposes of
which are hopefully adequately defined in this document. Broadly speaking,
there are three basic kinds: Production, Staging and Development.

# Production

While our ideal situation would be a single production deployment, differing
requirements from CRASH and other targets mean that we actually need two.

## CRASH
The CRASH server is currently called `prod`, and is stored in the `prod`
folders. This is used by the CRASH team as a BuildStream artifact cache. Due to
constraints, we needed to have TLS authentication on push and pull requests to
the cache. This required an extra certificate/key pair and a couple more lines
in the nginx configuration. Of course, it also has a specific domain. This is
available at https://cache.aws.celduin.co.uk.

## Public
We also have a server used as a cache server by some open source projects. This
only required TLS authentication on push requests, which necessitated it being
separated out from the CRASH cache. The cluster_id is `public`, and definitions
are stored in the `public` folders. This is available at https://public.aws.celduin.co.uk.


# Staging

Obviously, we'd rather not just deploy a change without testing it first, which
means that we need an environment to test changes before we merge. This is
where `staging` comes in. This is intended to be an environment that changes
can be applied to in order to check that things won't break in production. At
the moment it matches the CRASH cache config, as this is the most complex
deployment. This is available at https://staging.aws.celduin.co.uk.


# Development

As the other environments have a terraform backend, as well as requiring Redis
and S3, none of them are suitable to test changes either quickly on a
developer's machine. As a result we define a `dev` deployment, which is
suitable for use on a developer's machine or to quickly provision a small
cluster in the cloud (for some value of "quickly"). This is very different to
the production environments, and uses different backends for the object store.


# The Future

We're hoping to use jsonnet to template this repo, and avoid all this
duplication of yaml. When this is implemented, we should be able to deploy to
any of these environments using the same definitions.
