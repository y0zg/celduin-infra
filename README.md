# Celduin-Infra

For the end to end kubernetes deployment of [Remote Execution API](https://github.com/bazelbuild/remote-apis) (REAPI) servers and supporting services.

[Buildbarn](https://github.com/buildbarn) is used as the REAPI server implementation.

## Deploying locally

To deploy celduin-infra to your local machine, you can use our `dev` deployments.

We recommend the use of `kind` or `minikube` to run a kubernetes cluster locally.

You will also require:
* Jsonnet
  - This may be available from your distribution, but if not you can install
    the binary release from the [jsonnet github](https://github.com/google/jsonnet/releases)
* Kubectl

To deploy:

```bash
mkdir -p target
jsonnet -m target kubernetes/clusters/dev/deployment.jsonnet
kubectl apply -f target/*-Namespace.json
kubectl apply -f target
```

## Deploying to AWS

[Coming soon!](https://gitlab.com/celduin/infrastructure/celduin-infra/-/issues/139)

## Celduin-infra clusters

Celduin-infra runs several small kubernetes clusters as part of our development effort.

It is also used to support other Celduin projects such as [BuildStream BSPs](https://gitlab.com/celduin/bsps) and using [BuildStream and Bazel together](https://gitlab.com/celduin/buildstream-bazel).

The [libreML](https://gitlab.com/libreml/libreml) project currently uses celduin-infra infrastructure.

We currently have a

- Staging cluster: with automatic deployments that occur with merges to [master](https://gitlab.com/celduin/infrastructure/celduin-infra/-/commits/master)
- A `prod` cluster: with secured read-only and read-write endpoints.
- A `public` cluster: with a secure read-write endpoint and an open read-only endpoint.

Both `prod` and `public` clusters have automatic deployments when [releases](https://gitlab.com/celduin/infrastructure/celduin-infra/-/releases) occur.

If you would like to assist in our development or testing efforts that would be [greatly appreciated](./CONTRIBUTING.md)!

Currently there is compatability with [BuildStream](https://www.buildstream.build/) (1.4.x and master), and [Bazel](https://bazel.build/) (>3.1.0). For all clients there is remote-cache support. Remote-execution support to come [very soon](https://gitlab.com/celduin/infrastructure/celduin-infra/-/milestones/14)!

## Monitoring

Cluster activity can be monitored through grafana dashboards and prometheus backends.

## Contact us

For more information, please contact us at #celduin on freenode!
