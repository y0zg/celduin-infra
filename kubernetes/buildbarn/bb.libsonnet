// bb.libsonnet
//
// A libsonnet library for defining buildbarn kubernetes deployments
//
// The `component` function takes some mixins as arguments. These should be
// created using the convenience functions provided (otherwise they won't work.)
// The function can be passed only a single mixin, not several. It is down to
// the user to construct this mixin in a suitable way.
//
// The nature of the API means that it's easier to compose things like lists of
// volumes before passing them in as a mixin, as we apply the mixin by
// overriding a field in the final object, and can't do this more than once. For
// example, if you need to add several volumes to a pod, you should create a
// list of the volumes before creating the mixin from this list, rather than
// adding several mixins.

local kube = import '../kube.libsonnet';

local JAEGER_PORT = 5775;
local PROMETHEUS_PORT = 7981;
local DEFAULT_FILE_SIZE = 16 * 1024 * 1024;

// Defines a jaeger-agent sidecar
//
// @param version The version of the docker image to use
// @param endpoint The port to use as an endpoint
//
// @return a pod spec for a jaeger sidecar
local _jaegertracing(version='1.17', endpoint=JAEGER_PORT) = {
  image: 'jaegertracing/jaeger-agent:' + version,
  name: 'jaeger-agent',
  ports: [
    {
      containerPort: endpoint,
      protocol: 'UDP',
    },
  ],
  args: [
    '--collector.host-port=jaeger-collector:14267',
  ],
};

// Defines a generic Jaeger configuration for Buildbarn elements
//
// @param jPort Port number on which the Jaeger sidecar listens to
local _jaegerConfig(jPort=JAEGER_PORT) = {
  agentEndpoint: 'localhost:' + jPort,
  always_sample: true,
};

// Defines the core elements used in any buildbarn element configuration
//
// @param jaeger Jaeger configuration object - see _jaegerConfig for more details
local _bbGlobalConfig(jaeger={}) = {
  global: {
    jaeger: jaeger,
  },
};

// Defines a shard in a shardingblobstore configuration
//
// See https://github.com/buildbarn/bb-storage/blob/master/pkg/proto/configuration/blobstore/blobstore.proto#L277
//
// @return an object for shardingBlobAccess configuration
local shard(backend, weight=1) = {
  backend: backend,
  weight: weight,
};

// Defines a shardingblobstore configuration
//
// See https://github.com/buildbarn/bb-storage/blob/master/pkg/proto/configuration/blobstore/blobstore.proto#L277
//
// @return an object for shardingBlobAccess configuration
local sharding(shards) = {
  sharding: {
    shards: shards,
  },
};

// Defines a circularblobstore configuration
//
// See https://github.com/buildbarn/bb-storage/blob/master/pkg/proto/configuration/blobstore/blobstore.proto#L117
//
// @return an object for circularBlobAccess configuration
local circular(directory, offsetFileSizeBytes=16 * 1024 * 1024, offsetCacheSize=10000, dataFileSizeBytes=10 * 1024 * 1024 * 1024, dataAllocationChunkSizeBytes=16 * 1024 * 1024, instances=['']) = {
  circular: {
    directory: directory,
    offsetFileSizeBytes: std.toString(offsetFileSizeBytes),
    offsetCacheSize: std.toString(offsetCacheSize),
    dataFileSizeBytes: std.toString(dataFileSizeBytes),
    dataAllocationChunkSizeBytes: std.toString(dataAllocationChunkSizeBytes),
    instances: instances,
  },
};

// Defines a circularblobstore configuration
//
// See https://github.com/buildbarn/bb-storage/blob/master/pkg/proto/configuration/blobstore/blobstore.proto#L313
//
// @return an object for sizeDistinguishing configuration
local sizeDistinguishingConfig(small, large, cutoffSizeBytes) = {
  sizeDistinguishing: {
    small: small,
    large: large,
    cutoffSizeBytes: std.toString(cutoffSizeBytes),
  },
};

// Defines a redis backend with a single endpoint
//
// See https://github.com/buildbarn/bb-storage/blob/master/pkg/proto/configuration/blobstore/blobstore.proto#L193
//
// @return an object for redis single configuration
local redisSingle(endpoint) = {
  redis: {
    single: {
      endpoint: endpoint,
    },
  },
};

// Defines a grpc client backend with no authentication
//
// See https://github.com/buildbarn/bb-storage/blob/master/pkg/proto/configuration/grpc/grpc.proto#L11
//
// @return an object for grpc configuration
local grpc(endpoint) = {
  grpc: {
    address: endpoint,
  },
};

// Defines a backend that will immediately return a gRPC error
//
// See https://github.com/buildbarn/bb-storage/blob/master/pkg/proto/configuration/grpc/grpc.proto#L11
//
// @return an object with configuration to return a gRPC error
local err(code) = {
  'error': {
    code: code,
  },
};

// Defines a S3 backend
//
// See https://github.com/buildbarn/bb-storage/blob/master/pkg/proto/configuration/blobstore/blobstore.proto#L256
//
// @return an object for configuration for an S3 backend
local S3(endpoint, region, bucket) = {
  cloud: {
    s3: {
      endpoint: endpoint,
      region: region,
      bucket: bucket,
    },
  },
};

// Defines an action cache configuration
//
// @param config The configuration
//
// @return a fully specified actionCache configuration
local actionCache(config) = {
  actionCache: config,
};

// Defines a content addressable storage configuration
//
// @param config The configuration
//
// @return a fully specified contentAddressableStorage configuration
local contentAddressableStorage(config) = {
  contentAddressableStorage: config,
};

// Defines a gRPC server, assumes that the listening address will be local
//
// @param endpoint The host port
// @param authenticationPolicy Optional. Defaults to allow all.
//
// @return a gRPC server for buildbarn to listen to
local grpcServer(endpoint, authenticationPolicy={ allow: {} }) = [
  {
    listenAddresses: [':' + endpoint],
    authenticationPolicy: authenticationPolicy,
  },
];

// Defines a generic bb-browser configuration
//
// @param name The name of the configuration as it will appear in Kubernetes
// @param blobstore The blobstore configuration for this bb-storage
//
// @return a fully specified bb-browser configuration
local bbBrowserConfig(blobstore, maxMessageSizeBytes) = {
  name: 'browser',
  data: std.toString(
    {
      blobstore: blobstore,
      maximumMessageSizeBytes: maxMessageSizeBytes,
    },
  ),
};

// Defines a bb-storage configuration
//
// Should be used in conjunction with bb.component
//
// @param name The name of the configmap
// @param blobstore The blobstore configuration
// @param grpcServers The configuration of bb-storage as a gRPC server
// @param promPort Optional. The port that bb-storage listens to for http services
// @param maxMessageSizeBytes Optional. maximum protobuf message size
// @param allowAcUpdatesForInstances Optional. For what instance name action cache updates are permitted.
// @param jaeger Jaeger configuration object - see _jaegerConfig for more details about it
//
// @return a fully specified bb-storage configuration
local bbStorageConfig(name,
                      blobstore,
                      grpcServers,
                      promPort=PROMETHEUS_PORT,
                      maxMessageSizeBytes=DEFAULT_FILE_SIZE,
                      allowAcUpdatesForInstances=[''],
                      jaeger=_jaegerConfig()) =
  {
    name: name,
    config:: _bbGlobalConfig(jaeger) {
      blobstore: blobstore,
      grpcServers: grpcServers,
      allowAcUpdatesForInstances: allowAcUpdatesForInstances,
      httpListenAddress: ':' + std.toString(promPort),
      maximumMessageSizeBytes: maxMessageSizeBytes,
    },
    data: std.toString($.config),
  };

// Defines a bb-frontend configuration
//
// @param name The name of the configmap
// @param grpcServers The configuration of bb-storage as a gRPC server
// @param schedulers List of bb-scheduler endpoints this bb-frontend can point to
// @param promPort Optional. The port that bb-storage listens to for http services
// @param maxMessageSizeBytes Optional. maximum protobuf message size
// @param allowAcUpdatesForInstances Optional. For what instance name action cache updates are permitted.
// @param jaeger Jaeger configuration object - see _jaegerConfig for more details about it
local bbFrontendConfig(name,
                       blobstore,
                       grpcServers,
                       schedulers,
                       promPort=PROMETHEUS_PORT,
                       maxMessageSizeBytes=DEFAULT_FILE_SIZE,
                       allowAcUpdatesForInstances=[''],
                       jaeger=_jaegerConfig) =
  bbStorageConfig(name, blobstore, grpcServers, promPort, maxMessageSizeBytes, allowAcUpdatesForInstances) {
    config+:: {
      schedulers: schedulers,
    },
  };

// Defines a bb-scheduler configuration
//
// @param name The name of the configmap
// @param contentAddressableStorage bb-storage configuration pointing at the blobstore
// @param demultiplexServers Port on which the demultiplex server listens
// @param workerServers the configuration to use when spawning bb-workers
// @param browserUrl Url to reach bb-browser
// @param promPort Optional. The port that bb-storage listens to for http services
// @param maxMessageSizeBytes Optional. maximum protobuf message size
// @param jaeger Jaeger configuration object - see _jaegerConfig for more details about it
local bbSchedulerConfig(name,
                        contentAddressableStorage,
                        demultiplexServers,
                        workerServers,
                        browserUrl,
                        promPort=PROMETHEUS_PORT,
                        maxMessageSizeBytes=DEFAULT_FILE_SIZE,
                        jaeger={}) =
  {
    name: name,
    data: std.toString(
      {
        contentAddressableStorage: contentAddressableStorage,
        browserUrl: browserUrl,
        clientGrpcServers: demultiplexServers,
        httpListenAddress: ':' + std.toString(promPort),
        maximumMessageSizeBytes: maxMessageSizeBytes,
        workerGrpcServers: workerServers,
      },
    ),
  };

// Defines a bb-worker configuration
//
// @param name The name of the configmap
// @param blobstore The blobstore configuration
// @param browserUrl Url to reach bb-browser
// @param scheduler Endpoint of the scheduler to which to connect
// @param instaceName Name to report to the scheduler
// @param platform Properties to be reported to the scheduler
// @param numRunners The amount of bb-runners allocated to the bb-worker
// @param defaultExecutionTimeout Execution timeout for build actions doesn't have one
// @param maximumExecutionTimeout Maximum tmeout execution timeout permitted
// @param promPort Optional. The port that bb-storage listens to for http services
// @param maxMessageSizeBytes Optional. maximum protobuf message size
// @param maximumCacheFileCount Maximum number of files in the cache
// @param maximumCacheSizeBytes Maximum size of the cache in Bytes
// @param maximumMemoryCachedDirectories Maximum number of directory listings to keep in memory
// @param jaeger Jaeger configuration object - see _jaegerConfig for more details about it
// @param cacheReplacementPolicy Name of the cache policy to use
local bbWorkerConfig(name,
                     blobstore,
                     browserUrl,
                     scheduler,
                     instanceName,
                     platform,
                     numRunners,
                     defaultExecutionTimeout='1800s',
                     maximumExecutionTimeout='3600s',
                     promPort=PROMETHEUS_PORT,
                     maxMessageSizeBytes=DEFAULT_FILE_SIZE,
                     maximumCacheFileCount=10000,
                     maximumCacheSizeBytes=1024 * 1024 * 1024,
                     maximumMemoryCachedDirectories=1000,
                     jaeger={},
                     cacheReplacementPolicy='LEAST_RECENTLY_USED',
                     chroot=false,
                     workerConfigMixin={}) =
  {
    workerDirectory:: '/worker',
    buildDirectoryPath:: '/worker/build',
    cacheDirectoryPath:: '/worker/cache',
    devices:: [],
    worker: {
      name: 'worker-' + name,
      data: std.toString(
        {
          blobstore: blobstore,
          browserUrl: browserUrl,
          scheduler: { address: scheduler },
          maximumMemoryCachedDirectories: maximumMemoryCachedDirectories,
          httpListenAddress: ':' + std.toString(promPort),
          maximumMessageSizeBytes: maxMessageSizeBytes,
          instanceName: instanceName,
          buildDirectories: [
            {
              native: {
                buildDirectoryPath: $.buildDirectoryPath,
                cacheDirectoryPath: $.cacheDirectoryPath,
                maximumCacheFileCount: maximumCacheFileCount,
                maximumCacheSizeBytes: maximumCacheSizeBytes,
                cacheReplacementPolicy: cacheReplacementPolicy,
              },
              runners: [
                {
                  endpoint: { address: 'unix:///worker/runner' + index },
                  concurrency: 1,
                  platform: {
                    properties: platform,
                  },
                  defaultExecutionTimeout: defaultExecutionTimeout,
                  maximumExecutionTimeout: maximumExecutionTimeout,
                  workerId: {
                    pod: 'POD',  // This gets replaced by using an init container
                    node: 'NODE',  // This gets replaced by using an init container
                    runner: '%d' % index,
                  },
                } + if std.length($.devices) != 0 then { devices: $.devices } else {}
                for index in std.range(0, numRunners - 1)
              ],
            },
          ],
        },
      ),
    },
    runners: [
      {
        name: 'runner' + index + '-' + name,
        data: std.toString(
          {
            buildDirectoryPath: $.buildDirectoryPath,
            grpcServers: [{
              listenPaths: ['/worker/runner' + index],
              authenticationPolicy: { allow: {} },
            }],
          } + if chroot then { chrootIntoInputRoot: true } else {}
        ),
      }
      for index in std.range(0, numRunners - 1)
    ],
  } + workerConfigMixin;

// Mixin to add a list of devices to the runners in the worker config
//
// @param devices A list of device names e.g. ['null', 'zero']
//
// @return a mixin to add devices to the runners in the worker config
local addFieldsToWorkerConfig(devices=[]) = {
  devices:: devices,
};


// Mixin to add a list of VolumeMounts to a pod
//
// It is down to the user to ensure that all of the volume mounts specified
// actually exist.
//
// @param volumeMounts A list of VolumeMount objects to be added.
//
// @return a mixin to add volume mounts to a pod
local addVolumeMountsToBuildbarnPod(volumeMounts) = {
  spec+: {
    bbStorageVolumeMount: volumeMounts,
  },
};

// Mixin to add VolumeClaims to a StatefulSet's VolumeClaimTemplates
//
// @param volumeClaims A list of VolumeClaims to add
//
// @return a mixin to add volume claims to a statefulset.
local addVolumeClaimsToBuildbarnStatefulSet(volumeClaims) = {
  spec+: {
    bbStorageVolumeClaimTemplate:: volumeClaims,
  },
};

// Mixin to add InitContainers to a pod
//
// @param initContainers A list of InitContainers to add
//
// @return a mixin to add initcontainers to a pod
local addInitContainersToBuildbarnPod(initContainers) = {
  spec+: {
    bbStorageInitContainers:: initContainers,
  },
};

// Mixin to add capabilities to container
//
// @param capabilities A list of capabilities to add
//
// @return a mixin to add capabilities to a container
local addCapabilitiesToContainer(capabilities) = {
  securityContext: {
    capabilities: {
      add: capabilities,
    },
  },
};

// Defines a buildbarn component with deployment, service and configmap
//
// @param name The name of the kubernetes deployment, service and configmap that configures the buildbarn component
// @param image The buildbarn container image
// @param config The configuration of bb-storage as a gRPC server
// @param namespace The namespace that the deployment will reside in.
// @param endpointPort The port open for gRPC services to listen from
// @param deploymentType The type of deployment to use (e.g. StatefulSet, Deployment)
// @param deploymentMixin A mixin to add to the deployment specification
// @param specificationMixin A mixin to add to the pod specification
// @param PROMETHEUS_PORT The port open for prometheus services to scrape from
// @param replicas The number of replicas. Defaults to 1.
// @param scheduler set to `true` when this is used to generate a bb-scheduler
// @param workerPort Port on which bb-workers are listening
// @param service Whether to provide a service for the buildbarn deployment Defaults to false.
//
// @return a configured buildbarn deployment, configmap and service (if specified)
local component(name,
                image,
                config,
                namespace,
                endpointPort,
                deploymentType=kube.deployment,
                deploymentMixin={},
                specificationMixin={},
                promPort=PROMETHEUS_PORT,
                replicas=1,
                scheduler=false,
                workerPort=0,
                service=false) = [
  deploymentType(name, namespace) {
    spec: {
      bbStorageVolumeClaimTemplate:: [],

      replicas: replicas,
      selector: {
        matchLabels: {
          app: name,
        },
      },
      template: {
        metadata: {
          labels: {
            app: name,
          },
          annotations: {
            prometheus_io_scrape: 'true',
            prometheus_io_port: std.toString(promPort),
          },
        },
        spec: {
          bbStorageVolumeMount:: [],
          bbStorageInitContainers:: [],
          containers: [
            local spec = self;
            {
              args: [
                '/config/bb_config.jsonnet',
              ],
              image: image,
              name: name,
              ports: [
                {
                  containerPort: endpointPort,
                  protocol: 'TCP',
                },
                {
                  containerPort: promPort,
                  protocol: 'TCP',
                },
              ] + (
                if scheduler then [
                  {
                    containerPort: workerPort,
                    protocol: 'TCP',
                  },
                ] else []
              ),
              volumeMounts: [
                {
                  mountPath: '/config',
                  name: 'config',
                },
              ] + spec.bbStorageVolumeMount,
            },
            _jaegertracing(),
          ],
          volumes: [
            {
              configMap: {
                name: config.name,
              },
              name: 'config',
            },
          ],
          initContainers: self.bbStorageInitContainers,
        },
      } + specificationMixin,
    } + if self.kind == 'StatefulSet' then { volumeClaimTemplates:
      self.bbStorageVolumeClaimTemplate, serviceName: name } else {},
  } + deploymentMixin,
  kube.configMap(config.name, namespace) {
    data: {
      'bb_config.jsonnet': config.data,
    },
  },
  if service then
    kube.service(name, namespace) {
      spec: {
        ports: [
          {
            name: 'endpoint',
            port: endpointPort,
            protocol: 'TCP',
          },
        ] + (
          if scheduler then [
            {
              name: 'worker-endpoint',
              port: workerPort,
              protocol: 'TCP',
            },
          ] else []
        ),
        selector: {
          app: name,
        },
      },
    },
];

// Generate Kubernetes definition of a buildbarn worker
//
// @param name The name of the kubernetes deployment, service and configmap that configures the buildbarn component
// @param workerImage The container image for the worker
// @param runerImage The container image for the runner
// @param workerConfig The configuration of the worker
// @param namespace The namespace that the deployment will reside in.
// @param endpointPort The port open for gRPC services to listen from
// @param deploymentType The type of deployment to use (e.g. StatefulSet, Deployment)
// @param deploymentMixin A mixin to add to the deployment specification
// @param specificationMixin A mixin to add to the pod specification
// @param promPort The port open for prometheus services to scrape from
// @param numWorkers The number of replicas
// @param service Whether to provide a service for this element
local worker(name,
             workerImage,
             runnerImage,
             workerConfig,
             namespace,
             endpointPort,
             deploymentType=kube.daemonSet,
             deploymentMixin={},
             specificationMixin={},
             promPort=PROMETHEUS_PORT,
             numWorkers=1,
             service=false,
             runnerMixin={}) = [
  deploymentType(name, namespace) {
    spec: {
      [if self.kind != 'DaemonSet' then 'replicas']: numWorkers,
      selector: {
        matchLabels: {
          app: name,
        },
      },
      template: {
        metadata: {
          labels: {
            app: name,
          },
          annotations: {
            prometheus_io_scrape: 'true',
            prometheus_io_port: std.toString(promPort),
          },
        },
        spec: {
          containers: [
            {
              args: [
                '%s/bb_worker.jsonnet' % workerConfig.workerDirectory,
              ],
              image: workerImage,
              name: name,
              ports: [
                {
                  containerPort: endpointPort,
                  protocol: 'TCP',
                },
                {
                  containerPort: promPort,
                  protocol: 'TCP',
                },
              ],
              volumeMounts: [
                kube.volumeMount('worker-directory', workerConfig.workerDirectory),
              ],
            },
            _jaegertracing(),
          ] + std.mapWithIndex(
            function(index, runner) {
              args: [
                '/config/bb_config.jsonnet',
              ],
              image: runnerImage,
              name: name + 'runner-' + index,
              volumeMounts: [
                {
                  mountPath: '/config',
                  name: 'runner-config' + index,
                },
                kube.volumeMount('worker-directory', workerConfig.workerDirectory),
              ],
            } + runnerMixin, workerConfig.runners
          ),
          volumes: [
            {
              configMap: {
                name: workerConfig.worker.name,
              },
              name: 'worker-config',
            },
            {
              emptyDir: {
                medium: 'Memory',
              },
              name: 'worker-directory',
            },
          ] + std.mapWithIndex(
            function(index, runner) {
              configMap: {
                name: runner.name,
              },
              name: 'runner-config' + index,
            }, workerConfig.runners,
          ),
          initContainers: [
            {
              name: 'worker-setup',
              image: 'busybox:1.30.1',
              command: [
                'sh',
                '-c',
                'mkdir -pm 0777 %s && mkdir -pm 0700 %s && chmod 0777 %s' % [
                  workerConfig.buildDirectoryPath,
                  workerConfig.cacheDirectoryPath,
                  workerConfig.workerDirectory,
                ],
              ],
              volumeMounts: [
                kube.volumeMount('worker-directory', workerConfig.workerDirectory),
              ],
            },
            {
              name: 'inject-vars',
              image: 'busybox:1.30.1',
              env: [
                {
                  name: 'NODE_NAME',
                  valueFrom: {
                    fieldRef: {
                      fieldPath: 'spec.nodeName',
                    },
                  },
                },
                {
                  name: 'POD_NAME',
                  valueFrom: {
                    fieldRef: {
                      fieldPath: 'metadata.name',
                    },
                  },
                },
              ],
              command: [
                'sh',
                '-c',
                'sed "s/POD/${POD_NAME}/g; s/NODE/${NODE_NAME}/g" /config/bb_config.jsonnet > %s/bb_worker.jsonnet' % workerConfig.workerDirectory,
              ],
              volumeMounts: [
                kube.volumeMount('worker-config', '/config'),
                kube.volumeMount('worker-directory', workerConfig.workerDirectory),
              ],
            },
          ],
        },
      } + specificationMixin,
    },
  } + deploymentMixin,
  kube.configMap(workerConfig.worker.name, namespace) {
    data: {
      'bb_config.jsonnet': workerConfig.worker.data,
    },
  },
  if service then
    kube.service(name, namespace) {
      spec: {
        ports: [
          {
            name: 'endpoint',
            port: endpointPort,
            protocol: 'TCP',
          },
        ],
        selector: {
          app: name,
        },
      },
    },
] + [
  kube.configMap(runner.name, namespace) {
    data: {
      'bb_config.jsonnet': runner.data,
    },
  }
  for runner in workerConfig.runners
];

{
  addCapabilitiesToContainer:: addCapabilitiesToContainer,
  addFieldsToWorkerConfig:: addFieldsToWorkerConfig,
  addInitContainersToBuildbarnPod:: addInitContainersToBuildbarnPod,
  addVolumeMountsToBuildbarnPod:: addVolumeMountsToBuildbarnPod,
  addVolumeClaimsToBuildbarnStatefulSet:: addVolumeClaimsToBuildbarnStatefulSet,
  component:: component,
  worker:: worker,
  bbStorageConfig:: bbStorageConfig,
  bbBrowserConfig:: bbBrowserConfig,
  bbFrontendConfig:: bbFrontendConfig,
  bbSchedulerConfig:: bbSchedulerConfig,
  bbWorkerConfig:: bbWorkerConfig,
  actionCache:: actionCache,
  contentAddressableStorage:: contentAddressableStorage,
  sizeDistinguishingConfig:: sizeDistinguishingConfig,
  redisSingle:: redisSingle,
  S3:: S3,
  circular:: circular,
  grpc:: grpc,
  err:: err,
  grpcServer:: grpcServer,
  sharding:: sharding,
  shard:: shard,
}
