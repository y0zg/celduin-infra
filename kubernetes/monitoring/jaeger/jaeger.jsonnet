local jaeger = import 'jaeger.libsonnet';
local name = {
  collector: 'jaeger-collector',
  query: 'jaeger-query',
  configMap: 'jaeger-configuration',
  cassandra: 'cassandra',
};
local namespace = 'rex';

jaeger.jaegerCollector(name.collector, namespace) +
jaeger.jaegerQuery(name.query, namespace) +
jaeger.jaegerConfigMap(name.configMap, namespace) +
jaeger.jaegerCassandra(name.cassandra, namespace)
