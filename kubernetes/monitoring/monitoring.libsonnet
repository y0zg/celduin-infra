// Monitoring Libsonnet
//
// Wrapper functions around the monitoring components, just to simplify the API
// more than anything.

local grafana = import 'grafana/grafana.libsonnet';
local jaeger = import 'jaeger/jaeger.libsonnet';
local prometheus = import 'prometheus/prometheus.libsonnet';

// Create a grafana deployment
//
// @param namespace The namespace for the deployment
// @param frontendDomain The domain for the ingress, used for redirects
// @param image The docker image to use
//
// @return a fully configured grafana deployment
local _grafanaDeployment(namespace, frontendDomain, image) =
  local config = grafana.config('grafana', frontendDomain, anonymousAuth=true);
  local promDatasource = grafana.datasource('prometheus-datasource', 'http://prometheus.rex:9090/prometheus');

  local bbDashboard = import 'grafana/dashboards/Buildbarn-dashboard.libsonnet';
  local bbThroughputDashboard = import 'grafana/dashboards/Buildbarn-Throughput-dashboard.libsonnet';
  local k8sDashboard = import 'grafana/dashboards/Kubernetes-dashboard.libsonnet';

  local dashboards = [
    db.dashboard(db.name, 'prometheus-datasource')
    for db in [
      { name: 'buildbarn-dashboard', dashboard: bbDashboard.dashboard },
      { name: 'buildbarn-throughput-dashboard', dashboard: bbThroughputDashboard.dashboard },
      { name: 'kubernetes-dashboard', dashboard: k8sDashboard.dashboard },
    ]
  ];

  grafana.deployment('grafana', image, config, namespace, 3000, datasources=[promDatasource], dashboards=dashboards, service=true);


// Create a jaeger deployment
//
// @param namespace The namespace for the deployment
//
// @return a fully configured jaeger deployment
local _jaegerDeployment(namespace) =
  jaeger.jaegerCollector('jaeger-collector', namespace) +
  jaeger.jaegerQuery('jaeger-query', namespace) +
  jaeger.jaegerConfigMap('jaeger-config', namespace) +
  jaeger.jaegerCassandra('jaeger-cassandra', namespace);

// Create a prometheus deployment, complete with node-exporter
//
// @param namespace The namespace for the deployment
// @param frontendDomain The domain for the ingress, used for redirects
// @param prometheusImage The prometheus docker image to use
// @param nodeExporterImage The node-exporter docker image to use
//
// @return a fully configured Prometheus deployment
local _prometheusDeployment(namespace,
                            frontendDomain,
                            prometheusImage,
                            nodeExporterImage) =
  local config = prometheus.config('prometheus');
  local args = prometheus.promArgs(frontendDomain);

  prometheus.deployment('prometheus',
                        prometheusImage,
                        config,
                        namespace,
                        9090,
                        prometheusArgs=args,
                        service=true) +
  prometheus.nodeExporter('node-exporter',
                          nodeExporterImage,
                          namespace,
                          service=true);

// Create a full monitoring stack deployment
//
// @param namespace The namespace for the deployment
// @param frontendDomain The domain for the ingress, used for redirects
// @param grafanaImage The grafana docker image to use
// @param prometheusImage The prometheus docker image to use
// @param nodeExporterImage The node-exporter docker image to use
//
// @return a fully configured monitoring stack
local deployment(namespace,
                 frontendDomain,
                 grafanaImage,
                 prometheusImage,
                 nodeExporterImage) =
  _grafanaDeployment(namespace, frontendDomain, grafanaImage) +
  _prometheusDeployment(namespace,
                        frontendDomain,
                        prometheusImage,
                        nodeExporterImage);

{
  deployment:: deployment,
}
