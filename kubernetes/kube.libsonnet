// Kubernetes Libsonnet
//
// Abstractions for Kubernetes objects.
//
// Chiefly exists to simplify APIs and remove boilerplate.


// Template for a generic Kubernetes object
//
// @param apiVersion The API version for the component
// @param kind The kind of kubernetes object this is
// @param name The name of the object
// @param namespace The namespace for the object
// @param labels The labels for the object
//
// @return a boilerplate kubernetes object.
local _kubernetesObject(apiVersion, kind, name, namespace, labels={}) = {
  apiVersion: apiVersion,
  kind: kind,
  metadata: {
              name: name,
              namespace: namespace,
            }
            + (if labels == {} then {} else { labels: labels }),
};

// Template for a Kubernetes namespace
//
// @param namespace The name of the namespace to create
//
// @return a boilerplate Namespace definition
local namespace(namespace) = {
  apiVersion: 'v1',
  kind: 'Namespace',
  metadata: {
    name: namespace,
  },
};

// Template for a Kubernetes Deployment configuration
//
// @param name The name of this deployment
// @param namespace The namespace for the deployment
// @params labels The labels for the deployment
//
// @return a boilerplate Deployment definition
local deployment(name, namespace, labels={}) =
  _kubernetesObject(
    apiVersion='apps/v1',
    kind='Deployment',
    name=name,
    namespace=namespace,
    labels=labels,
  );

// Template for a Kubernetes ConfigMap configuration
//
// @param name The name of this configmap
// @param namespace The namespace for the configmap
// @params labels The labels for the configmap
//
// @return a boilerplate ConfigMap definition
local configMap(name, namespace, labels={}) =
  _kubernetesObject(
    apiVersion='v1',
    kind='ConfigMap',
    name=name,
    namespace=namespace,
    labels=labels,
  );

// Template for a Kubernetes Service configuration
//
// @param name The name of this service
// @param namespace The namespace for the service
// @params labels The labels for the service
//
// @return a boilerplate Service definition
local service(name, namespace, labels={}) =
  _kubernetesObject(
    apiVersion='v1',
    kind='Service',
    name=name,
    namespace=namespace,
    labels=labels,
  );

// Template for a Kubernetes DaemonSet configuration
//
// @param name The name of this DaemonSet
// @param namespace The namespace for the DaemonSet
//
// @return a boilerplate DaemonSet definition
local daemonSet(name, namespace) =
  _kubernetesObject(
    apiVersion='apps/v1',
    kind='DaemonSet',
    name=name,
    namespace=namespace,
  );

// Template for a Kubernetes ServiceAccount configuration
//
// @param name The name of this service account
// @param namespace The namespace for the service account
//
// @return a boilerplate ServiceAccount definition
local serviceAccount(name, namespace) =
  _kubernetesObject(
    apiVersion='v1',
    kind='ServiceAccount',
    name=name,
    namespace=namespace,
  );

// Template for an RBAC ClusterRole configuration
//
// @param name The name of this Cluster Role
//
// @return a boilerplate ClusterRole definition
local clusterRole(name) = {
  apiVersion: 'rbac.authorization.k8s.io/v1beta1',
  kind: 'ClusterRole',
  metadata: {
    name: name,
  },
};

// Template for an RBAC ClusterRoleBinding configuration
//
// @param name The name of this Cluster Role Binding
//
// @return a boilerplate ClusterRoleBinding definition
local clusterRoleBinding(name) = {
  apiVersion: 'rbac.authorization.k8s.io/v1beta1',
  kind: 'ClusterRoleBinding',
  metadata: {
    name: name,
  },
};

// Template for an container configuration
//
// @param name The name of container
// @param image Desired image to be deployed
//
// @return a boilerplate container definition
local container(name, image) = {
  name: name,
  image: image,
};

// Template for an StatefulSet configuration
//
// @param name The name of statefulSet
// @param namespace The namespace for the statefulSet
// @params labels The labels for the StatefulSet
//
// @return a boilerplate statefulSet definition
local statefulSet(name, namespace, labels={}) =
  _kubernetesObject(
    apiVersion='apps/v1',
    kind='StatefulSet',
    name=name,
    namespace=namespace,
    labels=labels,
  );

// Template for a job configuration
//
// @param name The name of job
// @param namespace The namespace for the job
// @params labels The labels for the job
//
// @return a boilerplate job definition
local job(name, namespace, labels={}) =
  _kubernetesObject(
    apiVersion='batch/v1',
    kind='Job',
    name=name,
    namespace=namespace,
    labels=labels,
  );

// Template for a generic Persistent Volume
//
// @param name The name of the volume
// @param namespace The namespace for the volume
// @param labels Optionally add labels to the object
//
// @return a persistent volume config
local persistentVolume(name, namespace, labels={}) =
  _kubernetesObject(
    apiVersion='v1',
    kind='PersistentVolume',
    name=name,
    namespace=namespace,
    labels=labels,
  );

// Template for a Persistent Volume Claim
//
// @param name The name for this claim
// @param namespace The namespace for this claim
// @param labels Optionally add labels to the object
//
// @return a persistent volume claim configuration
local persistentVolumeClaim(name, namespace, labels={}) =
  _kubernetesObject(
    apiVersion='v1',
    kind='PersistentVolumeClaim',
    name=name,
    namespace=namespace,
    labels=labels,
  );

// Template for a volume mount object
//
// @param name The name of the volume
// @param mountPath The path to mount the volume on
//
// @return a volume mount config
local volumeMount(name, mountPath) = {
  name: name,
  mountPath: mountPath,
};

{
  clusterRole:: clusterRole,
  clusterRoleBinding:: clusterRoleBinding,
  configMap:: configMap,
  deployment:: deployment,
  daemonSet:: daemonSet,
  namespace:: namespace,
  service:: service,
  serviceAccount:: serviceAccount,
  container:: container,
  statefulSet:: statefulSet,
  job:: job,
  persistentVolume:: persistentVolume,
  persistentVolumeClaim:: persistentVolumeClaim,
  volumeMount:: volumeMount,
}
