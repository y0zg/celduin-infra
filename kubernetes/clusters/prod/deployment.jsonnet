function(withCerts=false)
  local bb = import '../../buildbarn/bb.libsonnet';
  local cluster = import '../cluster.libsonnet';
  local certs = if withCerts then
    import 'config/certs-ConfigMap.jsonnet'
  else
    {};

  local tf = import '_tfOutput.jsonnet';

  cluster.deployment(
    namespace='rex',
    certs=certs,
    targetRevision='prod',
    statsDomain=tf.stats_domain.value,
    readOnlyDomain=tf.read_only_domain.value,
    readWriteDomain=tf.read_write_domain.value,
  )
