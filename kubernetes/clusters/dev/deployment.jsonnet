local bb = import '../../buildbarn/bb.libsonnet';
local cluster = import '../cluster.libsonnet';
local certs = import 'config/certs-ConfigMap.jsonnet';

cluster.deployment(
  namespace='rex',
  certs=certs,
  targetRevision='dev',
  statsDomain='stats.cache.com',
  readOnlyDomain='pull.cache.com',
  readWriteDomain='push.cache.com',
  shardSize=31,
)
