// cluster.libsonnet
//
// For a complete deployment of the rex + loadbalancer kubernetes stacks

local argo = import '../argo-cd/argo-cd.libsonnet';
local bb = import '../buildbarn/bb.libsonnet';
local kube = import '../kube.libsonnet';
local nginx = import '../loadbalancer/nginx.libsonnet';
local monitoring = import '../monitoring/monitoring.libsonnet';
local rex = import '../rex/rex.libsonnet';

local bbPort = rex.config.bbStorageEndpointPort;

local artifactPort = rex.config.artifactCacheEndpointPort;

// Utility to create an endpoint from a domain and a port
//
// @param domain The domain
// @param port The port
//
// @returns String of the form "domain:port"
local _constructEndpoint(domain, port) = domain + ':' + port;

// Wrapper around nginx config to simplify API
//
// See ../loadbalancer/nginx.libsonnet
local _configNginx(statsDomain,
                   readOnlyDomain,
                   readWriteDomain,
                   readOnlySSL,
                   readWriteSSL) = nginx.config(
  casEndpoint=_constructEndpoint(rex.config.demultiplexEndpoint, bbPort),
  artifactEndpoint=_constructEndpoint(rex.config.artifactCacheEndpoint, artifactPort),
  forbiddenEndpoint=_constructEndpoint(rex.config.forbiddenEndpoint, bbPort),
  browserEndpoint='http://browser.rex:80',
  grafanaEndpoint='http://grafana.rex:3000/',
  prometheusEndpoint='http://prometheus.rex:9090/prometheus/',
  jaegerEndpoint='http://jaeger-query.rex:16686/jaeger/',
  statsDomain=statsDomain,
  readOnlyDomain=readOnlyDomain,
  readWriteDomain=readWriteDomain,
  readOnlySSL=readOnlySSL,
  readWriteSSL=readWriteSSL,
);

// Returns a complete kubernetes deployment of rex + nginx
//
// Also creates the kubernetes namespace
//
// @param namespace The name of the namespace.
// @param certs The certificates required for TLS authentication
// @param statsDomain The domain for accessing all monitoring services on the cluster
// @param readOnlyDomain The domain for accessing the read-only endpoint for the cache service
// @param readWriteDomain The domain for accessing the read-write endpoint for the cache service
// @param targetRevision The target revision for argo-cd
// @param readOnlySSL Whether the read-only endpoint should be secured with SSL. Defaults to true
// @param readWriteSSL Whether the read-write endpoint should be secured with SSL. Defaults to true
// @param numShards The number of shards to be used in the sharding storage
// @param shardSize The size of each shard's storage.
//
// @return an array with preconfigured kubernetes deployment of rex + nginx
local deployment(namespace,
                 certs,
                 statsDomain,
                 readOnlyDomain,
                 readWriteDomain,
                 targetRevision,
                 readOnlySSL=true,
                 readWriteSSL=true,
                 numShards=3,
                 shardSize=250) =
  {
    ['%s-%s.json' % [kubeObject.metadata.name, kubeObject.kind]]: kubeObject
    for kubeObject in
      std.flattenArrays([
        [kube.namespace(namespace)],
        [
          argo.project(
            name='celduin-infra',
            description='Project representing Celduin kubernetes infrastructure',
            destinations='%s|https://kubernetes.default.svc,argocd|https://kubernetes.default.svc' % namespace,
          ),
          argo.application(
            name=namespace,
            project='celduin-infra',
            targetRevision=targetRevision,
            path='kubernetes/complete-solution',  //TODO: Remove this hardcoding.
            finalizer=true,
            autoSync=true,
            prune=true,
            namespace=namespace,
            server='https://kubernetes.default.svc',
          ),
        ],
        rex.deployment(
          namespace,
          bbStorageImage='registry.gitlab.com/celduin/infrastructure/celduin-infra/bb-storage-bst',
          bbBrowserImage='buildbarn/bb-browser:20200102T103638Z-e432c91',
          artifactCacheImage='buildstream/buildstream:dev',
          schedulerImage='buildbarn/bb-scheduler:20200402T064744Z-1497158',
          platforms=[
            rex.platform(
              name='ubuntu-16-04',
              properties=[
                { name: 'OSFamily', value: 'Linux' },
                { name: 'container-image', value: 'docker://marketplace.gcr.io/google/rbe-ubuntu16-04@sha256:6ad1d0883742bfd30eba81e292c135b95067a6706f3587498374a083b7073cb9' },
              ],
              workerImage='buildbarn/bb-worker:20200514T171931Z-2cb6875',
              runnerImage='buildbarn/bb-runner-ubuntu16-04:20200514T171931Z-2cb6875',
            ),
            rex.platform(
              name='buildstream',
              properties=[
                { name: 'ISA', value: 'x86-64' },
                { name: 'OSFamily', value: 'linux' },
              ],
              workerImage='registry.gitlab.com/celduin/infrastructure/celduin-infra/bb-worker-bst',
              runnerImage='registry.gitlab.com/celduin/infrastructure/celduin-infra/bb-runner-bst',
              capabilities=['SYS_CHROOT'],
              chroot=true,
              workerConfigMixin=bb.addFieldsToWorkerConfig(
                devices=['null', 'zero', 'random', 'full']
              ),
            ),
          ],
          numShards=numShards,
          shardSize=shardSize,
        ),
        nginx.deployment(
          namespace=namespace,
          certs=certs,
          config=_configNginx(statsDomain, readOnlyDomain, readWriteDomain, readOnlySSL, readWriteSSL),
          replicas=3,
          nginxVersion='1.17',
          promExporterVersion='0.6.0',
          httpNodeport=30080,
          httpsNodeport=30443,
        ),
        monitoring.deployment(
          namespace=namespace,
          frontendDomain='nginx.' + namespace,
          grafanaImage='grafana/grafana:6.6.2',
          prometheusImage='prom/prometheus:latest',
          nodeExporterImage='prom/node-exporter:latest',
        ),
      ])
    if kubeObject != null
  };

{
  deployment:: deployment,
}
