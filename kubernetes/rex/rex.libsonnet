// rex.libsonnet
//
// For deploying the microservices corresponding to the REAPI stack

local bb = import '../buildbarn/bb.libsonnet';
local kube = import '../kube.libsonnet';
local artifactCache = import 'artifactCache.libsonnet';

// The port that the bb-storage daemon listens at
local BB_STORAGE_ENDPOINT_PORT = 5501;

// The port that the bb-worker daemon listens at
local BB_WORKER_ENDPOINT_PORT = 5502;

// The port that the artifact cache daemon listens at
local ARTIFACT_CACHE_ENDPOINT_PORT = 1101;

// The port that bb-browser listens at
local BB_BROWSER_ENDPOINT_PORT = 80;

// The endpoint of the artifact cache
local ARTIFACT_CACHE_ENDPOINT = 'storage-artifact';

// The endpoint of bb-storage acting as the demultiplex
local DEMULTIPLEX_ENDPOINT = 'demultiplex';

// The endpoint of bb-scheduler acting as the scheduler
local SCHEDULER_ENDPOINT = 'scheduler';

// the endpoint of bb-storage acting as the storage backend
local STORAGE_ENDPOINT = 'storage-cas';

// The endpoint of bb-storage acting as a deny endpoint
local FORBIDDEN_ENDPOINT = 'forbidden';

// The endpoint of bb-browser
local BROWSER_ENDPOINT = 'browser';

// The maximum size used for messages sent to bb-storage
local MAX_MESSAGE_SIZE_BYTES = 16 * 1024 * 1024;

local REMOTE_EXECUTION_INSTANCE = 'remote-execution';

// Create shards for a ShardingBlobAccess, of equal weight. Each shard is a gRPC
// BlobAccess pointing to a pod in a StatefulSet.
//
// @param storageEndpoint The endpoint for the storage
// @param storageEndpointPort The port the backend bb-storage is listening on
// @param numShards The number of storages in use
local _shards(storageEndpoint, numShards, storageEndpointPort=BB_STORAGE_ENDPOINT_PORT) = [
  bb.shard(bb.grpc('%s-%d.%s:%s' % [storageEndpoint, n, storageEndpoint, storageEndpointPort]))
  for n in std.range(0, numShards - 1)
];

// Create several persistent volume claims, with an identical size, along with
// volumeMounts for these claims, mounting them to `/$name`
//
// @param names List of names for the volume claims
// @param namespace Namespace for the volume claims
// @param storageSize The size of the volume claims in GiB
//
// @returns a bb-storage config to just return gRPC PERMISSION_DENIED
// Creates kubernetes components for each kind of bb-storage required
// @param an array of Persistent Volume Claim configs
local _storageVolumes(names, namespace, storageSize=250) =
  {
    volumeClaims: [
      {
        metadata: {
          name: name,
        },
        spec: {
          accessModes: [
            'ReadWriteOnce',
          ],
          resources: {
            requests: {
              storage: '%dGi' % storageSize,
            },
          },
        },
      }
      for name in names
    ],
    volumeMounts: [
      kube.volumeMount(name, '/%s' % name)
      for name in names
    ],
  };

// Creates kubernetes components for a remote execution deployment
//
// @param namespace The namespace to use for the deployment
// @param storageImage The bb-storage image to use
// @param schedulerImage The bb-scheduler image to use
// @param platforms An array of platforms that are used to define a group of workers.
// @param numShards The number of shards to use in the sharding backend
// @param shardSize The size in GiB of each shard
// @param statelessReplicas The number of replicas for each demultiplex/forbidden proxy.
//
// @returns an array of kubernetes definitions for all the buildbarn components
//          in a deployment
local _buildbarnComponents(namespace,
                           storageImage,
                           browserImage,
                           schedulerImage,
                           platforms,
                           numShards=3,
                           shardSize=250,
                           statelessReplicas=3) = {
  local blobstore = bb.actionCache(bb.sharding(_shards(STORAGE_ENDPOINT,
                                                       numShards))) +
                    bb.contentAddressableStorage(bb.sharding(_shards(STORAGE_ENDPOINT,
                                                                     numShards))),

  local bbStorageListener = bb.grpcServer(endpoint=BB_STORAGE_ENDPOINT_PORT),
  local bbWorkerListener = bb.grpcServer(endpoint=BB_WORKER_ENDPOINT_PORT),
  components:: std.flattenArrays(
    [
      bb.component(
        name=bbStorage.name,
        image=storageImage,
        config=bbStorage.data,
        namespace=namespace,
        endpointPort=BB_STORAGE_ENDPOINT_PORT,
        replicas=bbStorage.replicas,
        service=true,
        specificationMixin=bbStorage.specMixin,
        deploymentMixin=bbStorage.deploymentMixin,
        deploymentType=bbStorage.deploymentType,
      )
      for bbStorage in [
        {
          // This facilitates a 30GiB buffer between the size allocated
          // for the shard, and what is allocated as the datafile size.
          assert shardSize > 30,
          local storageVols = _storageVolumes(['storage'], namespace, storageSize=shardSize),
          name: STORAGE_ENDPOINT,
          data: bb.bbStorageConfig(
            name='storage',
            blobstore=bb.actionCache(bb.circular('/storage/ac', instances=['%s' % REMOTE_EXECUTION_INSTANCE] + [''])) +
                      bb.contentAddressableStorage(
                        bb.circular(
                          directory='/storage/cas',
                          offsetFileSizeBytes=7 * 1024 * 1024 * 1024,
                          dataFileSizeBytes=(shardSize - 30) * 1024 * 1024 * 1024,
                        )
                      ),
            grpcServers=bb.grpcServer(
              endpoint=BB_STORAGE_ENDPOINT_PORT,
            ),
            allowAcUpdatesForInstances=['%s' % REMOTE_EXECUTION_INSTANCE] + [''],
          ),
          replicas: numShards,
          specMixin: bb.addVolumeMountsToBuildbarnPod(volumeMounts=storageVols.volumeMounts) +
                     bb.addInitContainersToBuildbarnPod(initContainers=[
                       {
                         name: 'setup-storage',
                         image: 'busybox:1.28',
                         command: ['sh', '-c', 'mkdir -p /storage/ac /storage/cas'],
                         volumeMounts: storageVols.volumeMounts,
                       },
                     ]),
          deploymentMixin: bb.addVolumeClaimsToBuildbarnStatefulSet(volumeClaims=storageVols.volumeClaims),
          deploymentType: kube.statefulSet,
        },
        {
          name: DEMULTIPLEX_ENDPOINT,
          data: bb.bbFrontendConfig(
            name='demultiplex',
            blobstore=blobstore,
            grpcServers=bbStorageListener,
            schedulers={
              ['%s' % REMOTE_EXECUTION_INSTANCE]: { address: SCHEDULER_ENDPOINT + ':' + BB_STORAGE_ENDPOINT_PORT },
            },
            allowAcUpdatesForInstances=['%s' % REMOTE_EXECUTION_INSTANCE] + [''],
          ),
          specMixin: {},
          deploymentMixin: {},
          replicas: statelessReplicas,
          deploymentType: kube.deployment,
        },
        {
          local err = bb.err(7),  // PERMISSION_DENIED gRPC error
          name: FORBIDDEN_ENDPOINT,
          data: bb.bbStorageConfig(
            name='forbidden',
            blobstore=bb.actionCache(err) + bb.contentAddressableStorage(err),
            grpcServers=bbStorageListener,
          ),
          specMixin: {},
          deploymentMixin: {},
          replicas: statelessReplicas,
          deploymentType: kube.deployment,
        },
      ]
    ] + [
      bb.component(
        name=SCHEDULER_ENDPOINT,
        image=schedulerImage,
        config=bb.bbSchedulerConfig(
          name='scheduler',
          contentAddressableStorage=bb.sharding(_shards(STORAGE_ENDPOINT, numShards)),
          demultiplexServers=bbStorageListener,
          workerServers=bbWorkerListener,
          browserUrl='foo',
        ),
        namespace=namespace,
        endpointPort=BB_STORAGE_ENDPOINT_PORT,
        workerPort=BB_WORKER_ENDPOINT_PORT,
        scheduler=true,
        service=true,
      ),
      bb.component(
        name=BROWSER_ENDPOINT,
        image=browserImage,
        config=bb.bbBrowserConfig(
          blobstore=blobstore,
          maxMessageSizeBytes=MAX_MESSAGE_SIZE_BYTES,
        ),
        namespace=namespace,
        endpointPort=BB_BROWSER_ENDPOINT_PORT,
        workerPort=BB_WORKER_ENDPOINT_PORT,
        service=true,
      ),
    ] + [
      bb.worker(
        name=platform.name,
        workerImage=platform.workerImage,
        runnerImage=platform.runnerImage,
        workerConfig=bb.bbWorkerConfig(
          name=platform.name,
          blobstore=blobstore,
          browserUrl='foo',
          scheduler=SCHEDULER_ENDPOINT + ':' + BB_WORKER_ENDPOINT_PORT,
          instanceName=REMOTE_EXECUTION_INSTANCE,
          platform=platform.properties,
          numRunners=platform.numRunners,
          chroot=platform.chroot,
          workerConfigMixin=platform.workerConfigMixin,
        ),
        namespace=namespace,
        endpointPort=BB_WORKER_ENDPOINT_PORT,
        service=true,
        runnerMixin=if std.length(platform.capabilities) != 0 then
          bb.addCapabilitiesToContainer(platform.capabilities) else {}
      )
      for platform in platforms
    ],
  ),
};

// Defines a worker platform
//
// @param name The name of the platform
// @param properties An array of key/value pairs defining the platform. Also see
//             https://github.com/bazelbuild/remote-apis/blob/master/build/bazel/remote/execution/v2/remote_execution.proto#L573
// @param numRunners The number of runners per worker. Defaults to 8.
// @param workerImage The docker image to use for the worker
// @param runnerImage The docker image to use for the runner
local platform(name,
               properties,
               numRunners=3,
               workerImage,
               runnerImage,
               capabilities=[],
               chroot=false,
               workerConfigMixin={}) = {
  name: name,
  properties: properties,
  numRunners: numRunners,
  workerImage: workerImage,
  runnerImage: runnerImage,
  capabilities: capabilities,
  chroot: chroot,
  workerConfigMixin: workerConfigMixin,
};

// Returns an array of kubernetes objects corresponding to a remote execution deployment
//
// @param namespace The kubernetes namespace that the rex stack should reside in
// @param bbStorageImage The bb-storage image to use
// @param artifactCacheImage The image and tag of the artifact cache
// @param schedulerImage The bb-scheduler image to use
// @param workerImage The bb-worker image to use
// @param runnerImage The bb-runner image to use
// @param platforms The platforms that are used to define a group of workers
// @param numShards The number of shards to use in the sharding backend
// @param bbBrowserImage The image and tag of the bb-browser docker image to use
// @param shardSize The size in GiB of each shard)
//
// @return an array with preconfigured buildstream artifact cache and buildbarn components
local deployment(namespace,
                 bbStorageImage,
                 bbBrowserImage,
                 artifactCacheImage,
                 schedulerImage,
                 platforms,
                 numShards=3,
                 shardSize=250) = std.flattenArrays([
  artifactCache.deployment(name=ARTIFACT_CACHE_ENDPOINT, namespace=namespace, image=artifactCacheImage, port=ARTIFACT_CACHE_ENDPOINT_PORT),
  _buildbarnComponents(namespace,
                       storageImage=bbStorageImage,
                       browserImage=bbBrowserImage,
                       schedulerImage=schedulerImage,
                       platforms=platforms,
                       numShards=numShards,
                       shardSize=shardSize).components,
]);

{
  deployment:: deployment,
  platform:: platform,
  config:: {
    artifactCacheEndpoint:: ARTIFACT_CACHE_ENDPOINT,
    artifactCacheEndpointPort:: ARTIFACT_CACHE_ENDPOINT_PORT,
    bbStorageEndpointPort:: BB_STORAGE_ENDPOINT_PORT,
    bbBrowserEndpointPort:: BB_BROWSER_ENDPOINT_PORT,
    demultiplexEndpoint:: DEMULTIPLEX_ENDPOINT,
    storageEndpoint:: STORAGE_ENDPOINT,
    forbiddenEndpoint:: FORBIDDEN_ENDPOINT,
    browserEndpoint:: BROWSER_ENDPOINT,
  },
}
