// artifactCache.libsonnet
//
// For deploying a buildstream artifact cache

// Returns buildstream artifact cache stateful set + ClusterIP service
//
// This must exist as a stateful singleton
//
// @param name The name of the deployment + service
// @param namespace The namespace that this resides in
// @param image The container image of the buildstream artifact cache
// @param port The port that the daemon listens at
//
// @return an array corresponding to a buildstream artifact cache deployment + service
local deployment(name, namespace, image, port) = [
  {
    apiVersion: 'apps/v1',
    kind: 'StatefulSet',
    metadata: {
      name: name,
      namespace: namespace,
    },
    spec: {
      replicas: 1,
      selector: {
        matchLabels: {
          app: name,
        },
      },
      serviceName: name,
      template: {
        metadata: {
          labels: {
            app: name,
          },
        },
        spec: {
          containers: [
            {
              command: [
                'bst-artifact-server',
                '--port',
                std.toString(port),
                '--index-only',
                '--log-level',
                'trace',
                '--enable-push',
                '/artifacts',
              ],
              image: image,
              name: 'artifact-server',
              ports: [
                {
                  containerPort: port,
                  protocol: 'TCP',
                },
              ],
              volumeMounts: [
                {
                  mountPath: '/artifacts',
                  name: 'artifacts',
                },
              ],
            },
          ],
        },
      },
      volumeClaimTemplates: [
        {
          metadata: {
            name: 'artifacts',
          },
          spec: {
            accessModes: [
              'ReadWriteOnce',
            ],
            resources: {
              requests: {
                storage: '1Gi',
              },
            },
          },
        },
      ],
    },
  },
  {
    apiVersion: 'v1',
    kind: 'Service',
    metadata: {
      name: name,
      namespace: namespace,
    },
    spec: {
      ports: [
        {
          name: 'artifact-store',
          port: port,
          protocol: 'TCP',
        },
      ],
      selector: {
        app: name,
      },
    },
  },
];

{
  deployment:: deployment,
}
