# Kubernetes Deployments

This folder contains a Kubernetes deployment of a remote cache compatible with
BuildStream and (I assume) bazel, RECC and anything else that can push to
bb-storage.

This is basically just a bb-storage and a bst-artifact-server hiding behind
nginx, which does some gRPC routing. To use this simply point your client
(e.g. BuildStream) at the nginx endpoint (you shouldn't need to specify a port)
and run a build.

This cache is compatible (for now) with BuildStream 1.4.x and master!

To deploy, configure your `kubectl` to where you want to deploy, and run

```bash
mkdir -p target
jsonnet -m target clusters/$environment/deployment.jsonnet
kubectl apply -f target/*-namespace.json -f target/
```

This will first create the necessary namespace, then load in the configs, and
finally create the pods and services we run.

## Specifics
There are two endpoints specified in nginx, but both using the same services.
One is a public facing "read only" cache, whereas the other is allowed to have
new artifacts pushed. The "push" cache is protected by SSL authentication,
which will need to be specified client side. The certs are stored in certs.yml.

Nginx serves as a gRPC router, which sends the correct requests to the correct
place. It also stops write requests being sent from the read only config.

Behind nginx there are 3 services running. First, a bb-storage that just
returns a gRPC error to anything that tries to contact it, which is where
illegal write requests get routed. Second we have a bst-artifact-server, to
store the references/artifact protos. Finally there's a bb-storage for the
CAS backend.

There is also a monitoring stack behind the same nginx.

```mermaid
graph LR;
  %% Nodes
  nginx[Nginx]:::loadBalancer;
  bb-forbidden[bb-storage Forbidden]:::service;
  bst-artifact-server[BuildStream Artifact Server]:::service;
  bb-storage[bb-storage CAS and Action Cache]:::service;
  disk1[(On Disk Storage)]:::storage;
  disk2[(On Disk Storage)]:::storage;

  %% Edges
  nginx -- Auth Failed --> bb-forbidden
  nginx -- buildstream/ --> bst-artifact-server --> disk1;
  nginx -- build.bazel* --> bb-storage --> disk2;
  nginx -- google.bytestream* --> bb-storage;

  %% Styling
  classDef loadBalancer fill:#591,stroke:#333;
  classDef service fill:#f75,stroke:#333;
  classDef storage fill:#57f,stroke:#333;
```

## Secrets
The secrets in this folder are currently encrypted using ansible vault. The
password may be obtained from @coldtom, if needed.

## Libsonnet Library Structure

This folder allows you to deploy remote cache server to a kubernetes cluster,
all the possible cluster environments have been stored under `clusters/` folder
and the details of the deployment can be seen in the `deployment.jsonnet` file
of each setup (`clusters/${ENVIRONMENT}/deployment.jsonnet`).

In order to create the cluster deployments, we have created a set of libraries
where all the needed components for the cluster are defined. This libraries are
stored within the directories here, organized by their porpoise.

Currently we have the next libsonnet libraries:
  - `kube.libsonnet`.
  - `bb.libsonnet`.
  - `cluster.libsonnet`.
  - `nginx.libsonnet`.
  - `grafana.libsonet`.
  - `jaeger.libsonnet`.
  - `prometheus.libsonnet`.
  - `artifactCache.libsonnet`.
  - `rex.libsonnet`.

Each of them define a set of APIs that help create a remote execution cluster
with a monitoring stack. Let's go in detail on what each of them provide:

### kube.libsonnet
This library is the core over which the rest of the libraries are built. It
defines a set of abstractions for Kubernetes objects such as deployments,
services, namespaces etc. Exposes a set of APIs to be used:

  - **clusterRole**: Template for an RBAC ClusterRole configuration.
  - **clusterRoleBinding**: Template for an RBAC ClusterRoleBinding configuration.
  - **configMap**: Template for a Kubernetes ConfigMap configuration.
  - **deployment**: Template for a Kubernetes Deployment configuration.
  - **daemonSet**: Template for a Kubernetes DaemonSet configuration.
  - **namespace**: Template for a Kubernetes namespace.
  - **service**: Template for a Kubernetes Service configuration.
  - **serviceAccount**: Template for a Kubernetes ServiceAccount configuration.
  - **container**: Template for a Kubernetes container configuration.
  - **statefulSet**: Template for a statefulSet container configuration.
  - **job**: Template for a job container configuration.

### bb.libsonnet
This library defines a set of buildbarn Kubernetes components. It's based in
`kube.libsonnet`. Exposes a set of APIs to be used:

  - **component**: Defines a buildbarn component with deployment, service and
    configmap.
  - **bbStorageConfig**: Defines a bb-storage configuration.
  - **actionCache**: Defines an action cache configuration.
  - **contentAddressableStorage**: Defines a content addressable storage.
    configuration.
  - **sizeDistinguishingConfig**: Defines a circularblobstore configuration.
  - **redisSingle**: Defines a redis backend with a single endpoint.
  - **S3**: Defines a S3 backend.
  - **circular**: Defines a circularblobstore configuration.
  - **grpc**: Defines a grpc client backend with no authentication.
  - **err**: Defines a backend that will immediately return a gRPC error.
  - **grpcServer**: Defines a gRPC server..

### nginx.libsonnet
Based on `kube.libsonnet`, provides configuration of the nginx loadbalancer that
serves as the entrypoint for the cluster. Exposes the next APIs:

  - **config**: Configures the nginx loadbalancer configMap.
  - **deployment**: Returns an array of objects corresponding to an nginx
    kubernetes deployment.

### artifactCache.libsonnet:
Self-contained, provides a mechanism to deploy a buildstream artifact cache.
Exposes the next API:

  - **deployment**:  Returns buildstream artifact cache stateful set + ClusterIP
    service.

### rex.libsonnet:
Based on `kube.libsonnet`, `bb.libsonnet` and `artifactCache.libsonnet`,
provides a mechanism for deploying the microservices corresponding to the REAPI
rex stack. Exposes the next APIs:

  - **deployment**: Returns an array of kubernetes objects corresponding to a
    remote cache deployment.
  - **config:: {..}**: Provides a list of configuration values. See the
    rex.libsonnet for the complete list.

### cluster.libsonnet
Based on `kube.libsonnet`, `ning.libsonnet`, `rex.libsonnet` and
`bb.libsonnet`. This library provides APIs for a complete deployment of
a rex cluster and loadbalancer kubernetes stack. From it have been
developed deployment jsonnet files for `dev`, `prod`, `public` and `staging`.

This deployment.jsonnets will create all the elements needed for a complete
deployment of a remote execution infrastructure based on the requirements of
each if the environments.

The cluster.libsonnet exposes the next APIs:

  - **deployment**: Returns a complete kubernetes deployment of
    rex + nginx.
  - **RedisS3Storage**: Returns a bb-storage blobstore sizeDistinguishing
    configuration with redis and S3 backends.

### grafana.libsonnet
Based on `kube.libsonnet`, is a template for Grafana configuration and
deployment. Exposes the next APIs:

  - **deployment**: Defines a full grafana deployment, including configuration
    and service, if specified.
  - **config**: Defines a new grafana configuration.

### jaeger.libsonnet
Based on `kube.libsonnet`, is a template for Jaeger configuration and
deployment. Exposes the next APIs:

  - **jaegerCollector**: Deploys jaeger collector.
  - **jaegerQuery**: Deploys Jaeger Query.
  - **jaegerConfigMap**: Creates a configmap for jaeger collector.
  - **jaegerCassandra**: Deploys Cassandra database.

### prometheus.libsonnet
Based on `kube.libsonnet`, provides templates and abstractions for Prometheus,
including the Node Exporter. Exposes the next APIs:

  - **config**: Template for Prometheus configuration.
  - **promArgs**: Generates a list of default arguments for prometheus.
  - **nodeExporter**: Creates a fully configured Node Exporter deployment.
  - **deployment**: Generates a fully configured prometheus deployment.

## monitoring.libsonnet
Based on `prometheus.libsonnet`, `jaeger.libsonnet` and `grafana.libsonnet`,
provides wrapper functions around the monitoring components. Exposes one API:
  - **deployment**: Create a full monitoring stack deployment.

```mermaid
  graph TD
  A[kube.libsonnet] --> B(bb.libsonnet)
  A --> C[nginx.libsonnet]
  D[artifactCache.libsonnet]
  C --> E[cluster.libsonnet]
  D --> F[rex.libsonnet]
  C --> F
  B --> F
  F --> E
  A --> G[prometheus.libsonnet]
  A --> H[jaeger.libsonnet]
  A --> I[grafana.libsonnet]
  G --> J[monitoring.libsonnet]
  H --> J
  I --> J
  J --> E
```
