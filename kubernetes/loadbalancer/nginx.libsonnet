// nginx.libsonnet
//
// For configuration of the nginx loadbalancer that serves as the entrypoint for the cluster

local kube = import '../kube.libsonnet';

// Configures the nginx loadbalancer configMap
//
// This should be passed to the config parameter of the nginx.deployment() function
//
// @param casEndpoint The endpoint for bb-storage acting as demultiplex
// @param artifactEndpoint The endpoint for the buildstream artifact cache
// @param forbiddenEndpoint The endpoint for bb-storage acting as a deny endpoint
// @param grafanaEndpoint The endpoint for the grafana service
// @param prometheusEndpoint The endpoint for the prometheus service
// @param jaegerEndpoint The endpoint for the jaeger service
// @param statsDomain The domain for accessing all monitoring services on the cluster
// @param readOnlyDomain The domain for accessing the read-only endpoint for the cache service
// @param readOnlySSL Whether the read-only endpoint should be secured with SSL. Defaults to true
// @param readWriteDomain The domain for accessing the read-write endpoint for the cache service
// @param readWriteSSL Whether the read-write endpoint should be secured with SSL. Defaults to true
// @param workerConnections Maximum number of client connections permitted. Defaults to 4096
// @param clientMaxBodySize The maximum size of client request body. Defaults to no limit (0)
//
// @return an object corresponding to the data for the nginx loadbalancer configmap
local config(
  casEndpoint,
  artifactEndpoint,
  forbiddenEndpoint,
  browserEndpoint,
  grafanaEndpoint,
  prometheusEndpoint,
  jaegerEndpoint,
  statsDomain,
  readOnlyDomain,
  readOnlySSL=true,
  readWriteDomain,
  readWriteSSL=true,
  workerConnections=4096,
  clientMaxBodySize=0,
      ) = {
  casEndpoint:: casEndpoint,
  artifactEndpoint:: artifactEndpoint,
  forbiddenEndpoint:: forbiddenEndpoint,
  browserEndpoint:: browserEndpoint,
  grafanaEndpoint:: grafanaEndpoint,
  prometheusEndpoint:: prometheusEndpoint,
  jaegerEndpoint:: jaegerEndpoint,
  statsDomain:: statsDomain,
  readOnlyDomain:: readOnlyDomain,
  readOnlySSL:: if readOnlySSL then 'on' else 'off',
  read_only_cert:: if readOnlySSL then 'ssl_client_certificate /certs/client-pull.crt;' else '',
  readWriteDomain:: readWriteDomain,
  readWriteSSL:: if readWriteSSL then 'on' else 'off',
  read_write_cert:: if readWriteSSL then 'ssl_client_certificate /certs/client-push.crt;' else '',
  workerConnections:: workerConnections,
  clientMaxBodySize:: clientMaxBodySize,
  'nginx.conf': |||
    events {
      worker_connections    %(workerConnections)s;
    }
    http {
        client_max_body_size    %(clientMaxBodySize)s;  # Remove limit on request body size

        ##############################################
        # Define some upstreams to reduce duplication
        #
        upstream cas {
            zone cas 64k;
            server %(casEndpoint)s;
        }

        upstream artifacts {
            zone artifacts 64k;
            server %(artifactEndpoint)s;
        }

        upstream forbidden {
            zone forbidden 64k;
            server %(forbiddenEndpoint)s;
        }

        #####################
        # Prometheus Metrics
        #
        server {
            listen 8080;
            location /metrics {
                stub_status on;
            }
        }

        #########################
        # HTTP -> HTTPS redirect
        #
        server {
            listen 80 http2;
            return 301 https://$host$request_uri;
        }

        ###############################
        # For internal cluster traffic
        #
        server {
            listen 80;
            server_name nginx.rex;

            access_log 		/var/log/nginx/access.log;
            error_log		/var/log/nginx/error.log warn;

            location /grafana/ {
                proxy_pass      %(grafanaEndpoint)s;
            }

            location /prometheus/ {
                proxy_pass      %(prometheusEndpoint)s;
            }
        }

        ###########################
        # Monitoring Stack routing
        #
        server {
            listen 443 http2 ssl;
            server_name %(statsDomain)s;

            ssl_certificate	/certs/server.crt;
            ssl_certificate_key /certs/server.key;

            ssl_session_cache builtin:1000 shared:SSL:10m;
            ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
            ssl_ciphers 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256';
            ssl_prefer_server_ciphers on;

            access_log 		/var/log/nginx/access.log;
            error_log		/var/log/nginx/error.log warn;

            location /grafana/ {
                proxy_pass      %(grafanaEndpoint)s;
            }

            location /prometheus/ {
                proxy_pass      %(prometheusEndpoint)s;
            }
        }

        ##########################
        # Read Only Cache routing
        #
        server {
            listen 443 http2 ssl;
            server_name %(readOnlyDomain)s;

            ssl_certificate	/certs/server.crt;
            ssl_certificate_key /certs/server.key;

            %(read_only_cert)s
            ssl_verify_client %(readOnlySSL)s;

            ssl_session_cache builtin:1000 shared:SSL:10m;
            ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
            ssl_ciphers 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256';
            ssl_prefer_server_ciphers on;

            access_log 		/var/log/nginx/access.log;
            error_log		/var/log/nginx/error.log warn;

            grpc_read_timeout 600s;
            grpc_send_timeout 600s;
            client_body_timeout 600s;

            location = /buildstream.v2.ReferenceStorage/UpdateReference {
                grpc_pass       grpc://forbidden;
            }

            location = /buildstream.v2.ArtifactService/UpdateArtifact {
                grpc_pass       grpc://forbidden;
            }

            location = /buildstream.v2.SourceService/UpdateSource {
                grpc_pass       grpc://forbidden;
            }

            location /buildstream {
                grpc_pass	grpc://artifacts;
            }

            location = /build.bazel.remote.execution.v2.ContentAddressableStorage/BatchUpdateBlobs {
                grpc_pass	grpc://forbidden;
            }

            location /build.bazel {
                grpc_pass	grpc://cas;
            }

            location = /google.bytestream.ByteStream/Write {
                grpc_pass	grpc://forbidden;
            }

            location /google.bytestream {
                grpc_pass	grpc://cas;
            }

            location /browser/ {
                rewrite /browser/(.*) /$1  break;
                proxy_pass %(browserEndpoint)s;
            }
        }

        ###########################
        # Read/Write cache routing
        #
        server {
            listen 443 http2 ssl;
            server_name %(readWriteDomain)s;

            ssl_certificate	/certs/server.crt;
            ssl_certificate_key /certs/server.key;

            %(read_write_cert)s
            ssl_verify_client %(readWriteSSL)s;

            ssl_session_cache builtin:1000 shared:SSL:10m;
            ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
            ssl_ciphers 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256';
            ssl_prefer_server_ciphers on;

            access_log 		/var/log/nginx/access.log;
            error_log		/var/log/nginx/error.log warn;

            grpc_read_timeout 600s;
            grpc_send_timeout 600s;
            client_body_timeout 600s;

            location /buildstream {
                grpc_pass	grpc://artifacts;
            }

            location /build.bazel {
                grpc_pass	grpc://cas;
            }

            location /google.bytestream {
                grpc_pass       grpc://cas;
            }
        }
    }
  ||| % self,
};

// Returns an array of objects corresponding to an nginx kubernetes deployment
//
// @param name The name of the deployment. Defaults to nginx
// @param config The configuration of the nginx deployment. Please use nginx.config()
// @param certs The TLS certificates to secure access to services
// @param namespace The namespace that the nginx deployment resides in
// @param replicas The number of nginx
// @param nginxVersion The version of the nginx container image to use.
// @param promExporterVersion The version of the nginx prometheus exporter container image to use.
// @param httpNodeport The nodePort that nginx should listen at for http services
// @param httpsNodeport The nodeport that nginx should listen at for https services
//
// @return an object corresponding to the data for the nginx loadbalancer configmap
local deployment(name='nginx', config, certs, namespace, replicas, nginxVersion, promExporterVersion, httpNodeport, httpsNodeport) = [
  kube.deployment(name, namespace) {
    spec: {
      replicas: replicas,
      selector: {
        matchLabels: {
          app: name,
        },
      },
      template: {
        metadata: {
          labels: {
            app: name,
          },
          annotations: {
            prometheus_io_scrape: 'true',
            prometheus_io_port: '9113',
          },
        },
        spec: {
          containers: [
            {
              image: 'nginx:' + nginxVersion,
              name: name,
              ports: [
                {
                  containerPort: 80,
                  protocol: 'TCP',
                },
                {
                  containerPort: 8080,
                  protocol: 'TCP',
                },
                {
                  containerPort: 443,
                  protocol: 'TCP',
                },
              ],
              volumeMounts: [
                {
                  mountPath: '/etc/nginx/',
                  name: 'config',
                },
                {
                  mountPath: '/certs',
                  name: 'certs',
                },
              ],
            },
            {
              image: 'nginx/nginx-prometheus-exporter:' + promExporterVersion,
              name: 'nginx-prom-exporter',
              env: [
                {
                  name: 'SCRAPE_URI',
                  value: 'http://localhost:8080/metrics',
                },
                {
                  name: 'NGINX_RETRIES',
                  value: '10',
                },
              ],
              ports: [
                {
                  containerPort: 9113,
                  protocol: 'TCP',
                },
              ],
            },
          ],
          volumes: [
            {
              configMap: {
                name: 'nginx',
              },
              name: 'config',
            },
            {
              configMap: {
                name: 'certs',
              },
              name: 'certs',
            },
          ],
        },
      },
    },
  },
  kube.service(name, namespace) {
    spec: {
      ports: [
        {
          name: 'http',
          port: 80,
          targetPort: 80,
          nodePort: httpNodeport,
          protocol: 'TCP',
        },
        {
          name: 'https',
          port: 443,
          targetPort: 443,
          nodePort: httpsNodeport,
          protocol: 'TCP',
        },
      ],
      selector: {
        app: name,
      },
      type: 'NodePort',
    },
  },
  kube.configMap('nginx', namespace) {
    data: config,
  },
  if certs != {} then
    kube.configMap('certs', namespace) {
      data: certs,
    },
];

{
  config:: config,
  deployment:: deployment,
}
