This are some basic instructions to deploy Zuul in a kubernetes cluster in
minikube:

# Install kubectl

Follow the steps described here:
https://kubernetes.io/docs/tasks/tools/install-kubectl/

# Install Minikube:

Follow the steps described here:
https://kubernetes.io/docs/tasks/tools/install-minikube/

# Start a minikube cluster

> minikube start

# Deploy zuul components

The deployment of the zuul components needs to follow the next order:
1. namespace.yaml
2. Rest of components: configmaps (etc_zuul.yaml and playbooks.yaml),
   zookeeper.yaml, mariadb.yaml, zuul-scheduler.yaml, zuul-executor.yaml

To do so:

> kubectl apply -f namespace.yaml

> kubectl apply -f etc_zuul.yaml

> kubectl apply -f playbooks.yaml

> kubectl apply -f etc_zuul.yaml

> kubectl apply -f playbooks.yaml

> kubectl apply -f zookeeper.yaml

> kubectl apply -f mariadb.yaml

> kubectl -f zuul-scheduler.yaml

> kubectl apply -f zuul-executor.yaml
