{
  provider: {
    aws: {
      region: 'eu-west-1',
      version: '>= 2.11',
    },
  },
  resource: {
    aws_route53_zone: {
      aws_celduin_co_uk: {
        name: 'aws.celduin.co.uk',
      },
    },
    aws_s3_bucket: {
      object_bucket: {
        acl: 'private',
        bucket: 'celduin-tf-state',
      },
    },
  },
  terraform: {
    backend: {
      s3: {
        bucket: 'celduin-tf-state',
        endpoint: 's3.eu-west-1.amazonaws.com',
        key: 'global/terraform.tfstate',
        region: 'eu-west-1',
        skip_credentials_validation: true,
        skip_get_ec2_platforms: true,
        skip_metadata_api_check: true,
        skip_requesting_account_id: true,
      },
    },
    required_version: '>= 0.12.0',
  },
}
