// Dev Terraform
//
// Terraform to create a dev cluster

local Aws = import 'lib/aws.libsonnet';
local Tf = import 'lib/terraform.libsonnet';

local DevConfig = Aws.newK8sCluster('dev', [Aws.nodeGroupConfig('m5.large')]) +
                  Aws.newAwsProvider() +
                  Tf.newTfBlock();

Tf.newTfFile(DevConfig)
