// util Libsonnet
//
// Libsonnet to hold some utility functions

// Sanitises a hostname into valid terraform
//
// Just replaces '.' and '-' with '_' for now
//
// @param hostname The hostname to sanitise
//
// @return a sanitised version of the hostname
local SanitiseHostname(hostname) =
  std.strReplace(
    std.strReplace(hostname, '.', '_'), '-', '_'
  );

// Little convenience wrapper to represent a Kubernetes Label on a node in a
// user friendly way
//
// @param key The key for the label
// @param value The value for the label
//
// @return a representation of a label
local nodeLabel(key, value) =
  {
    key: key,
    value: value,
  };

// Converts a list of nodeLabels into a string which can be passed to kubelet.
//
// @param nodeLabels List of labels to apply
//
// @return a formatted string to be passed to --nodelabels=
local nodeLabelsToKubeletArgs(nodeLabels) =
  '--node-labels=' + std.join(',', ['%s=%s' % [l.key, l.value] for l in nodeLabels]);


{
  SanitiseHostname:: SanitiseHostname,
  nodeLabel:: nodeLabel,
  nodeLabelsToKubeletArgs:: nodeLabelsToKubeletArgs,
}
