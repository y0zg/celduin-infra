// Celduin Libsonnet
//
// Leverages our terraform libsonnets to template out entire standalone // deployments. For example the newCacheCluster() function templates an entire
// remote cache deployment.

local Aws = import 'aws.libsonnet';
local Tf = import 'terraform.libsonnet';

// Templates an entire remote cache deployment, including DNS
//
// @param cluster_id A unique ID for the cluster, used to namespace generated tf
// @param dns_zone The DNS hosted zone for the DNS names
// @param nodeGroups A list of NodeGroup configurations to be passed to the EKS
//                   cluster. See AWS libsonnet for details
// @param redis_instance The type of instance for redis
// @param dns_subdomain Optionally specify the subdomain to use. Defaults to
//                      cluster_id
//
// @return a full set of configs for a remote cache deployment
local newCacheCluster(cluster_id,
                      dns_zone,
                      nodeGroups=[Aws.nodeGroupConfig('m5.xlarge')],
                      redis_instance=null,
                      dns_subdomain=null,
                      backend_bucket_name='celduin-tf-state') =

  local subdomain =
    if dns_subdomain == null then
      cluster_id
    else
      dns_subdomain;


  Aws.newK8sCluster(cluster_id, nodeGroups) +

  Aws.newDnsRecord('%s.%s' % [subdomain, dns_zone],
                   dns_zone,
                   'CNAME',
                   ['${aws_lb.%s_nlb.dns_name}' % cluster_id],
                   ttl=300,
                   output_name='read_only_domain',) +

  Aws.newDnsRecord('push.%s.%s' % [subdomain, dns_zone],
                   dns_zone,
                   'CNAME',
                   ['${aws_lb.%s_nlb.dns_name}' % cluster_id],
                   ttl=300,
                   output_name='read_write_domain',) +

  Aws.newDnsRecord('stats.%s.%s' % [subdomain, dns_zone],
                   dns_zone,
                   'CNAME',
                   ['${aws_lb.%s_nlb.dns_name}' % cluster_id],
                   ttl=300,
                   output_name='stats_domain',) +

  Aws.newAwsProvider() +

  Tf.newTfBlock() +

  Tf.newS3Backend(
    backend_bucket_name,
    'remotecache/%s/terraform.tfstate' % cluster_id,
    's3.eu-west-1.amazonaws.com',
    'eu-west-1',
  );

{
  newCacheCluster: newCacheCluster,
  newTfFile: Tf.newTfFile,
}
