// Staging Terraform
//
// Terraform to create the staging cluster
function(dns_zone='aws.celduin.co.uk', backend_bucket_name='celduin-tf-state')
  local Celduin = import 'lib/celduin.libsonnet';
  local Aws = import 'lib/aws.libsonnet';

  local M5_LARGE_NODEGROUP = Aws.nodeGroupConfig('m5.large');
  local M5_XLARGE_NODEGROUP = Aws.nodeGroupConfig('m5.xlarge');

  local StagingConfig = Celduin.newCacheCluster(
    cluster_id='staging',
    dns_zone=dns_zone,
    nodeGroups=[
      M5_LARGE_NODEGROUP,
      M5_XLARGE_NODEGROUP,
    ],
    backend_bucket_name=backend_bucket_name,
  );


  Celduin.newTfFile(StagingConfig)
