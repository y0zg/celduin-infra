#!/usr/bin/env bash

# USAGE: ./scripts/terraform-jsonnet -e [environment] -c [terraform command]

# A quick script to convert the jsonnet terraform definitions to plain JSON
# and apply the terraform

set -ue

while getopts ":e:c:" opt; do
  case ${opt} in
    e ) ENVIRONMENT="$OPTARG";;
    c ) TERRAFORM_COMMAND="$OPTARG";;
    : ) echo "Missing argument for -$OPTARG" && exit 1;;
    \?) echo "./scripts/terraform-jsonnet -e [environment] -c [terraform command]" && exit 1;;
  esac
done

echo "Environment: $ENVIRONMENT, Terraform Command to execute: $TERRAFORM_COMMAND"

TERRAFORM_DIR=terraform
TERRAFORM_REQUIRED_VERSION="Terraform v0.12.24"
TMP_DIR=$(mktemp -d -t tmp.XXXXXXXXXX)
CURRENT_DIR=$(pwd)

jsonnet $TERRAFORM_DIR/$ENVIRONMENT.tf.jsonnet > $TMP_DIR/main.tf.json

if [[ `terraform --version | grep "${TERRAFORM_REQUIRED_VERSION}"` != $TERRAFORM_REQUIRED_VERSION ]]; then
  echo "Terraform version used must be $TERRAFORM_REQUIRED_VERSION"
  echo "This is so that tfState consumed is compatible with the terraform version used in CI"
  exit 1
fi

trap "cd $CURRENT_DIR" EXIT

# `terraform output` does not allow specifying a directory, so we may as well
# just run the entire thing from within the temporary directory...
cd $TMP_DIR
terraform init
terraform $TERRAFORM_COMMAND
terraform output -json > $CURRENT_DIR/kubernetes/clusters/$ENVIRONMENT/_tfOutput.jsonnet
