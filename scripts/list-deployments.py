#!/usr/bin/python3

import json
import sys
import os

target_dir = sys.argv[1]

for ent in os.listdir(target_dir):
    if ent.endswith(".json"):
        path = os.path.join(target_dir, ent)
        with open(path, "r") as f:
            data = json.load(f)
        if "kind" in data and data["kind"] in ("Deployment", "DaemonSet", "StatefulSet"):
            assert "metadata" in data
            meta = data["metadata"]
            print("{} {} {}".format(data["kind"], meta["name"], meta["namespace"]))

