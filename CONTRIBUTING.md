# Contributing

If you wish to assist in our development efforts with celduin-infra maintained clusters, this is the information you need.

## Pre commit hook

We provide a pre-commit hook in `scripts/pre-commit`, it's recommended to use
this to avoid committing secrets and to automatically run the jsonnet formatter.
I use
```bash
ln -s ../../scripts/pre-commit .git/hooks/pre-commit
```
to activate the hook.

## Deploying the infra

## Prerequisites

You will need:
* An AWS account with enough access to create resources
  - This can be obtained from @LucasHermann or @christopherphang
* The AWS CLI
  - This can be installed from PyPI - `pip3 install --user awscli`
* Terraform
  - Your distribution _may_ supply this, if not then you may simply download
    the binary from [hashicorp](https://www.terraform.io/downloads.html) and
    add it to your `$PATH`
* aws-iam-authenticator
  - This can be installed by following the
    [instructions](https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html)
* Jsonnet
  - This may be available from your distribution, but if not you can install
    the binary release from the [jsonnet github](https://github.com/google/jsonnet/releases)
* Kubectl (optional)

## Environments
We currently have a few terraform modules, first we have the `global` directory
which sets up the terraform state bucket, and the DNS hosted zone for our
domain. This should not be touched.

We then have production and staging environments. These should all be deployed
using CI and not manually. There are two production environments: `prod`, which
is used by the CRASH team; and `public`, a public cache intended for use by
projects who want it.

`staging` is what we use in CI, and may be possible to use for a quick test,
although having a `cache.t3.small` redis cache has caused us issues with
actually pushing blobs in the past. This should really be left to CI too.

`dev` is to be used by developers to quickly spin up a small test cluster, this
doesn't have a terraform backend, so as to avoid overwriting existing
infrastructure. This means you _must_ destroy the infra when done, otherwise
someone will need to hunt through AWS and delete things manually.

To create a new environment, _copy_ either `prod` or `staging`, and change the
`key` field in the `backend` of the `terraform` block. Not changing this will
overwrite currently deployed infra.

## Deploying

First you need to log in to AWS on the command line, using the awscli:
```bash
aws configure
```
and add in your access key ID, secret access key, and region. This will allow
terraform to provision infrastructure in AWS.

Your Access Key ID and Secret access key can be found from the AWS console in
your browser, under "My Security Credentials".
The region code can be found from the dropdown in the top-right corner of the AWS
console.


The `terraform/` folder contains the definitions for a remote BuildStream cache,
which is to say an EKS cluster, an Elasticache Redis instance, an S3 bucket and
a bit more surrounding infra. To deploy this, run the following from the project
root:

```bash
./scripts/terraform-jsonnet $environment
```


# Deploying the services

To deploy the services you need `kubectl` installed. This _may_ be packaged by
your distribution, so it's worth having a look, if not then you can follow the
instructions at https://kubernetes.io/docs/tasks/tools/install-kubectl/.

## Deploying from CI using ArgoCD

Celduin-infra uses ArgoCD to deploy kubernetes infrastructure. Upon merges to
master, rendered jsonnet will be pushed to a [separate repository](https://gitlab.com/celduin/infrastructure/infrastructure-declarations)

Celduin-infra does not describe the ArgoCD CRDs and other kubernetes objects required to deploy ArgoCD. To do so:

```bash
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/v1.4.2/manifests/install.yaml
```

If you need to remove ArgoCD resources, you can delete the `argocd` namespace. However due to finalizers
that can be deadlocked, the removal of the argocd namespace can hang. Following [this](https://github.com/kubernetes/kubernetes/issues/60538#issuecomment-369099998)
to patch the custom finalizer can faciliate the removal of all resources and deletion of the namespace.

```bash
kubectl delete crd applications.argoproj.io
# This will hang, Crtl-C to kill the current process whilst the deletion is stuck server side

kubectl patch crd applications.argoproj.io -p '{"metadata":{"finalizers":[]}}' --type=merge
# This will remove the offending finaliser

kubectl delete crd applications.argoproj.io

kubectl delete ns argocd
```


## Certificates

We need a few TLS certificates in order to 1) encrypt traffic and 2)
authenticate client access to the server. For production environments these are
encrypted using ansible-vault. For an example configmap, see `certs.yml` in
staging.

To generate a new "client" certificate/key pair, use:
```bash
openssl req -new -newkey rsa:4096 -x509 -sha256 -days 3650 -nodes -batch -subj "/CN=client" -out client.crt -keyout client.key
```

To generate a new "server" certificate/key pair, use:

```bash
certbot certonly --dns-route53 -d foo.aws.celduin.co.uk
```

Different deployments may have different numbers of certs, due to policy of who
can access the cache.

Deploying the certificates is an out of band operation, and should be done
manually for the time being. This can be done by generating the deployment json
as below, but adding the argument
```
--tla-code withCerts=true
```

to the jsonnet call, e.g.
```
jsonnet -m target --tla-code withCerts=true kubernetes/clusters/$environment/deployment.jsonnet
```

Note you will first need to decrypt the certificates (if this is prod, public or
staging) by using
```bash
ansible-vault decrypt kubernetes/clusters/$environment/config/certs-ConfigMap.jsonnet
```

and entering the password when prompted. This password may be obtained from
@coldtom.

## Deploying

You will need to configure kubectl to point at your cluster, to do this I like
to use
```bash
aws eks update-kubeconfig --name k8-cluster-$cluster_id
```
but you can just use the generated kubeconfig from terraform.

To deploy the kubernetes (once you have a `config/certs-ConfigMap.jsonnet` configmap in
place, see dev for an example), simply run
```bash
mkdir -p target
jsonnet -m target kubernetes/clusters/$environment/deployment.jsonnet
kubectl apply -f target/*-Namespace.json
kubectl apply -f target
```

In the event you need to deploy to staging, prod or public, you will need to
decrypt the certs first using
```bash
ansible-vault decrypt kubernetes/clusters/$environment/config/certs-ConfigMap.jsonnet
```

# Destroying Deployed Infra

To destroy, first make sure you delete all the services as Kubernetes in EKS can
provision volumes and load balancers, which can be left behind if you don't
clean up first. For this simply
```bash
mkdir -p target
jsonnet -m target kubernetes/clusters/$environment/deployment.jsonnet
kubectl delete -f target
```

Then navigate to the terraform deployed and
```bash
terraform destroy
```

# Formatting jsonnet

Our CI checks the format of our jsonnet files using `jsonnetfmt`. To ease
running this for the contributor, we provide a handy script to do this. Simply
run

```bash
./scripts/format-jsonnet
```
to format all jsonnet files in the repository.

# What's all this other stuff in the repo?

You may have noticed there are several folders here:

* `ansible` - legacy ansible, no longer used
* `docs` - documentation about what gets deployed and architecture
* `kubernetes` - definitions for kubernetes services
* `scripts` - scripts used for tasks, e.g. generating docs
* `terraform` - terraform definitions for infrastructure
* `zuul-deployment` - kubernetes for a zuul deployment, should be put in the
  `kubernetes/` directory in future
